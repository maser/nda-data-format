#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Ephemeris

    Adapted from B. Cecconi https://gitlab.obspm.fr/maser/catalogues/tfcat/-/blob/master/examples/leblanc_aa_catalogues/jupiter_ephem.py

"""


__author__ = 'Alan Loh'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'Jupiter'
    'JupiterMoon',
    'Sun',
]


from abc import ABC
from enum import Enum
from astropy.time import Time, TimeDelta
import astropy.units as u
from astropy.constants import c as light_speed
from sunpy.coordinates.sun import carrington_rotation_number
import numpy as np
from astropy.coordinates import (
    Longitude,
    Latitude,
    solar_system_ephemeris,
    get_body,
    SkyCoord,
    Angle
)
from webgeocalc import StateVector, GFCoordinateSearch
from requests.exceptions import ConnectionError

from pynda import LONGITUDE_NDA, NDA_LOCATION
from pynda.ephemeris import WEBGEOCALC_API

import logging
log = logging.getLogger(__name__)


# ============================================================= #
# Ehemeris Object
# ------------------------------------------------------------- #
def webgeocalc_position_query(time: Time, target: str, observer: str, frame: str, representation: str) -> dict:
    """ """
    try:
        calc = StateVector(
            api=WEBGEOCALC_API,
            kernels=1,
            times=time.isot if time.isscalar else time.isot.tolist(),
            target=target.upper(),
            observer=observer.upper(),
            reference_frame=frame.upper(),
            aberration_correction='LT',#_CORR[to_observer],
            state_representation=representation
        )
        return calc.run()
    except ConnectionError:
        log.error(f"Computing position of '{target}' as seen from '{observer}' has aborted due to Connection Error.")
        raise


class EphemerisObject(ABC):
    """ """

    @property
    def target_longitude(self) -> Longitude:
        """ Longitude is in self.reference_frame but centered on self.observer's location.
            Hence, Longitude of sub-observer point must be flipped by 180°
        """
        try:
            return self._target_longitude
        except AttributeError:
            log.error("Method `.compute_position()` needs to be called beforehand.")
            raise
    

    @property
    def target_latitude(self) -> Latitude:
        """ Latitude in deg
        """
        try:
            return self._target_latitude
        except AttributeError:
            log.error("Method `.compute_position()` needs to be called beforehand.")
            raise


    @property
    def target_subsolar_lat(self) -> Latitude:
        """ Latitude in deg
        """
        try:
            return self._target_subsolar_lat
        except AttributeError:
            log.error("Method `.compute_subsolar()` needs to be called beforehand.")
            raise


    @property
    def target_subsolar_lon(self) -> Latitude:
        """ Longitude in deg
        """
        try:
            return self._target_subsolar_lon
        except AttributeError:
            log.error("Method `.compute_subsolar()` needs to be called beforehand.")
            raise


    @property
    def target_time(self) -> Time:
        """ Time at target. """
        try:
            return self._target_time
        except AttributeError:
            log.error("Method `.compute_position()` needs to be called beforehand.")
            raise


    @property
    def target_distance(self) -> u.Quantity:
        """ Distance between observer and target """
        # light_speed_val = light_speed.to(u.m/u.s).value
        light_distance = [
            (u.s, u.m,  lambda x: light_speed*x, lambda x: x/light_speed)
        ]
        return self._target_light_time.to(u.m, equivalencies=light_distance)


    @property
    def target_local_time(self) -> Angle:
        """ Target local time with respect to the Sun. """
        result_sun = webgeocalc_position_query(
            time=self.time,
            target=self.target,
            observer='SUN',
            frame=self.reference_frame,
            representation='PLANETOGRAPHIC'
        )
        longitude_sun = Longitude(result_sun['LONGITUDE']*u.deg + 180*u.deg)

        return Angle(longitude_sun - self.target_longitude) + Angle(12, unit="hour")


    @property
    def cml(self) -> Longitude:
        """ Central Meridian Longitude.
            Reverses the sign, since CML is a west-ward longitude system.
        """
        return Longitude((360 - self.target_longitude.deg)%360, unit=u.deg)


    def next_meridian_transit(self) -> Time:
        """ Date and time of transit of the target at meridian, seen from Earth.
        """
        log.info(f"Computing passage of '{self.target}' at the meridian.")

        try:
            calc = GFCoordinateSearch(**self._prepare_query_meridian())
            log.info(calc.api)
            result = calc.run()
        except ConnectionError:
            log.error(f"Computing passage of '{self.target}' at the meridian has aborted due to Connection Error.")
            raise

        return Time(result["DATE"][0].replace("UTC", ""))


    def get_sky_coordinates(self) -> SkyCoord:
        """ """
        with solar_system_ephemeris.set('builtin'):
            source = get_body(
                body=self.target,
                time=self.time,
                location=NDA_LOCATION
            )
        # Return the SkyCoord instance converted to ICRS
        return SkyCoord(source.ra, source.dec)


    def compute_position(self) -> None:
        """ """
        log.info(f"Computing position of '{self.target}' as seen from '{self.observer}'.")

        result = webgeocalc_position_query(
            time=self.time,
            target=self.target,
            observer=self.observer,
            frame=self.reference_frame,
            representation='LATITUDINAL'
        )

        self._target_light_time = result['LIGHT_TIME']*u.s
        self._target_longitude = Longitude(result['LONGITUDE']*u.deg + 180*u.deg)
        self._target_latitude = Latitude(-(result['LATITUDE']*u.deg))
        
        try:
            self._target_time = Time(list(map(lambda t: t.replace('UTC', ''), result['TIME_AT_TARGET'])))
        except ValueError:
            self._target_time = Time(result['TIME_AT_TARGET'].replace('UTC', ''))


    def _prepare_query_meridian(self) -> dict:
        """ 
            See other https://webgeocalc.readthedocs.io/en/stable/calculation.html

            Perform the meridian transit query on start up to 48h to be sure to catch at least one transit.
        """
        if self.time.isscalar:
            time = self.time
        else:
            time = self.time[0]

        start_time = time
        end_time = time + TimeDelta(48*3600, format='sec')

        return {
            'api': WEBGEOCALC_API,
            'kernels':1,
            'intervals': [{'startTime': start_time.isot, 'endTime': end_time.isot}],
            'time_step': 1,
            'time_step_units': 'HOURS',
            'target': self.target,
            'observer': 'EARTH',
            'reference_frame': 'EARTH_FIXED',
            'aberration_correction': 'LT',
            'output_duration_units': 'SECONDS',
            'coordinate_system': 'PLANETOGRAPHIC',
            'coordinate': 'LONGITUDE',
            'relational_condition': '=',
            'reference_value': LONGITUDE_NDA.value
        }
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Jupiter class
# ------------------------------------------------------------- #
JUPITER_OBSERVERS = [
    'EARTH',
    'IO',
    'SUN',
    'GANYMEDE',
    'EUROPA',
    'CALLISTO',
    'AMALTHEA'
]
JUPITER_REFERENCE_FRAME = 'IAU_JUPITER'

class Jupiter(EphemerisObject):
    """ """

    def __init__(self, observer: str, time: Time):
        self.target = 'JUPITER'
        self.reference_frame = JUPITER_REFERENCE_FRAME
        self.observer = observer
        self.time = time

    @property
    def observer(self) -> str:
        """ Name of Jupiter moon. """
        return self._observer
    @observer.setter
    def observer(self, o: str):
        o = o.upper()
        if o not in JUPITER_OBSERVERS:
            raise NotImplementedError(
                f"No computation available for '{o}'. Please select an available observer: {JUPITER_OBSERVERS}."
            )
        self._observer = o


    @classmethod
    def seen_from(cls, observer: str, time: Time = Time.now()):
        """ """
        jupiter = cls(
            observer=observer,
            time=time
        )
        jupiter.compute_position()
        return jupiter
# ------------------------------------------------------------- #
# ============================================================= #

# ============================================================= #
# Jupiter Moon class
# ------------------------------------------------------------- #
class JupiterMoonObject(EphemerisObject):
    """ """

    def __init__(self, observer: str, time: Time):
        self.target = None
        self._jupiter = None
        self.reference_frame = JUPITER_REFERENCE_FRAME
        self.observer = observer
        self.time = time


    @property
    def target_mag_latitude(self) -> Latitude:
        """ Compute the magnetic latitude with model JRM33
        """
        mag_latitude = 10.25*np.cos(np.radians(197.09) - self.target_longitude.rad) + self.target_latitude.deg
        return Latitude(mag_latitude*u.deg)


    @property
    def moon_phase(self) -> Angle:
        """ Compute the phase of the Jupiter Moon.
        """
        return Angle(180. - self.target_longitude.deg + self._jupiter.cml.deg, unit='deg').wrap_at('360d', inplace=False)


    def compute_position(self) -> None:
        """ Bypass base class same method. """
        log.info(f"Computing position of '{self.target}' as seen from '{self.observer}'.")

        result_jup = webgeocalc_position_query(
            time=self.time,
            target=self.observer,
            observer='JUPITER',
            frame=self.reference_frame,
            representation='PLANETOGRAPHIC'
        )
        try:
            jupiter_time = Time(list(map(lambda t: t.replace('UTC', ''), result_jup['TIME_AT_TARGET'])))
        except ValueError:
            jupiter_time = Time(result_jup['TIME_AT_TARGET'].replace('UTC', ''))
        
        result = webgeocalc_position_query(
            time=jupiter_time,
            target='JUPITER',
            observer=self.target,
            frame=self.reference_frame,
            representation='PLANETOGRAPHIC'
        )

        self._target_light_time = result['LIGHT_TIME']*u.s
        self._target_longitude = Longitude(result['LONGITUDE']*u.deg + 180*u.deg)      
        self._target_latitude = Latitude((result['LATITUDE']*u.deg))

        try:
            self._target_time = Time(list(map(lambda t: t.replace('UTC', ''), result['TIME_AT_TARGET'])))
        except ValueError:
            self._target_time = Time(result['TIME_AT_TARGET'].replace('UTC', ''))
        

        self._jupiter = Jupiter.seen_from(observer=self.observer, time=self.time)


class JupiterMoon(Enum):
    """ """

    IO = 'IO'
    GANYMEDE = 'GANYMEDE'
    EUROPA = 'EUROPA'
    CALLISTO = 'CALLISTO'
    AMALTHEA = 'AMALTHEA'

    def __init__(self, target):
        self.target = target


    def seen_from(self, observer: str, time: Time = Time.now()) -> JupiterMoonObject:
        """ """

        moon = JupiterMoonObject(
            observer=observer,
            time=time
        )
        moon.target = self.target
        moon.compute_position()
        return moon
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Sun class
# ------------------------------------------------------------- #
SUN_OBSERVERS = [
    'EARTH',
    'JUPITER'
]
SUN_REFERENCE_FRAME = 'IAU_SUN'

class Sun(EphemerisObject):
    """ """

    def __init__(self, observer: str, time: Time):
        self.target = 'SUN'
        self.reference_frame = SUN_REFERENCE_FRAME
        self.observer = observer
        self.time = time


    @property
    def observer(self) -> str:
        """  """
        return self._observer
    @observer.setter
    def observer(self, o: str):
        o = o.upper()
        if o not in SUN_OBSERVERS:
            raise NotImplementedError(
                f"No computation available for '{o}'. Please select an available observer: {SUN_OBSERVERS}."
            )
        self._observer = o


    @classmethod
    def seen_from(cls, observer: str, time: Time = Time.now()):
        """ """
        sun = cls(
            observer=observer,
            time=time
        )
        sun.compute_position()
        return sun


    @staticmethod
    def carrington_rotation(time: Time) :
        return [carrington_rotation_number(time)]
# ------------------------------------------------------------- #
# ============================================================= #

