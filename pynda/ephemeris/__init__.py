#! /usr/bin/python3
# -*- coding: utf-8 -*-


from webgeocalc import Api

import logging
log = logging.getLogger(__name__)


try:
    # Using by default Paris Obs instance of webgeocalc
    WEBGEOCALC_API = 'http://voparis-webgeocalc2.obspm.fr:8080/geocalc/api'
    api = Api(WEBGEOCALC_API)
    kernels = api.kernel_sets()
    log.info(f"Using Webgeocalc API: '{WEBGEOCALC_API}'")
except:
    WEBGEOCALC_API = 'http://spice.esac.esa.int/webgeocalc/api'
    log.info(f"Using Webgeocalc API: '{WEBGEOCALC_API}'")

