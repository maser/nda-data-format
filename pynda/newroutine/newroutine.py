#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Reading and converting NDA/NewRoutine data to FITS
"""


__author__ = 'Alan Loh, Alexia Duchene'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh', 'Alexia Duchene']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'NDASlice',
    'NDA',
    'NDAFits'
]


import logging
import os
import re
import numpy as np
import json
import dask.array as da
from astropy.time import Time
import astropy.units as u
from astropy.io import fits
from packaging.specifiers import SpecifierSet

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.colors import LogNorm, Normalize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from pynda import NDA_RECEIVERS, SOURCE_CODE, QL_PARAMETERS
from pynda.pointing import NDAPointing
from pynda.newroutine.newroutinefits import NDAJupiterFits, NDASunFits, NDATransitFits

import logging
log = logging.getLogger(__name__)

with open(os.path.join(os.path.dirname(__file__), 'newroutine_fits_keywords.json'), 'r') as keyword_file:
    FITS_KWARGS = json.load(keyword_file)

# ============================================================= #
# NDASlice class
# ------------------------------------------------------------- #
class NDASlice:
    """ Slice of data in time/frequency space. """


    def __init__(self, time: Time, frequency: u.Quantity, data: np.ndarray, object: str):
        self.time = time
        self.frequency = frequency
        self.data = data
        self.object = object


    def time_rebin(self, n_time_min: int, n_time_max: int):
        """ """

        def get_divisors(n: int) -> np.ndarray:
            ''' Lists all the integer divisors of n. '''
            divisors = np.arange(1, n+1, dtype=int)
            return divisors[n%divisors == 0]

        time_size = self.time.size

        if time_size > n_time_min:
            # Progressively cropping the time in order to find
            # a number of time step that can be divided
            while True:
                factors = get_divisors(time_size)
                in_acceptable_range = (factors>=n_time_min) & (factors<=n_time_max)
                if np.any(in_acceptable_range):
                    n_time_final = factors[in_acceptable_range][0]
                    break
                time_size -= 1
        else :
            n_time_final = time_size
        step_size = int(time_size/n_time_final)

        log.info(f'Time rebin: the time dimension is cropped from {self.time.size} to {time_size}.')
        log.info(f'Time rebin: rebinning from {time_size} to {n_time_final} steps of size {step_size}...')

        time_rebin = self._rebin_array(
            array=self.time[0:time_size].jd,
            x_step=step_size
        )

        data_rebin = self._rebin_array(
            array=self.data[0:time_size, :],
            x_step=step_size
        )

        return NDASlice(
            time=Time(time_rebin, format='jd'),
            frequency=self.frequency,
            data=data_rebin,
            object=self.object
        )


    def plot_dynamic_spectrum(self,
            pointing: NDAPointing = None,
            fig_ax: tuple = None,
            **kwargs
        ) -> None:
        """
            kwargs:
                figsize
                vmin
                vmax
                percentile_clip_min
                percentile_clip_max
                cmap
                log
                cbar_label
                title
                decibel
        """

        if fig_ax is None:
            fig = plt.figure(figsize=kwargs.get('figsize', (15, 7)))
            ax = fig.add_subplot(111)
        else:
            fig, ax = fig_ax

        data = (10*np.log10(self.data)).compute() if kwargs.get('decibel', False) else self.data.compute()
        if pointing is not None:
            # Compute the percentile for f>flab
            threshold_fmin = max(pointing.lab_frequency)
            log.debug(f'Computing dynamic spectrum automatic value threshold above {threshold_fmin}.')
            f_mask = self.frequency >= threshold_fmin
        else:
            log.debug(f'Computing dynamic spectrum automatic value threshold forthe whole frequency range.')
            f_mask = None
        vmin = kwargs.get('vmin', np.nanpercentile(data[:, f_mask], kwargs.get('percentile_clip_min', 5)))
        vmax = kwargs.get('vmax', np.nanpercentile(data[:, f_mask], kwargs.get('percentile_clip_max', 95)))

        log.info(f'Plotting a dynamic spectrum of shape {self.data.shape}...')
        im = ax.pcolormesh(
            self.time.datetime,
            self.frequency.to(u.MHz).value,
            data.T,
            cmap=kwargs.get('cmap', 'viridis'),
            shading='auto',
            norm=LogNorm(vmin, vmax) if kwargs.get('log', False) else Normalize(vmin, vmax)
        )
        cax = inset_axes(
            ax,
            width='3%',
            height='100%',
            loc='lower left',
            bbox_to_anchor=(1.05, 0., 1, 1),
            bbox_transform=ax.transAxes,
            borderpad=0,
        )
        cbar = plt.colorbar(im, cax=cax)#, pad=0.03)
        cbar.set_label(
            kwargs.get('cbar_label', 'Colorbar TBD')
        )

        # if pointing is not None:
        #     pointing_times = self._match_pointing(pointing)
        #     self._display_pointing(ax, pointing_times)

        # X label
        ax.xaxis.set_major_formatter(
            mdates.DateFormatter('%H:%M:%S')
        )
        fig.autofmt_xdate()
        current_day = self.time[0].isot.split('T')[0]
        ax.set_xlabel(f'Hours of day {current_day} (UTC)')

        # Y label
        ax.set_ylabel(f'Frequency (MHz)')

        # Ticks
        ax.minorticks_on()

        # Title
        ax.set_title(kwargs.get('title', ''))

        if fig_ax is None:
            if kwargs.get('figname', '') != '':
                fig.savefig(
                    kwargs['figname'],
                    dpi=300,
                    bbox_inches='tight',
                    transparent=True
                )
                plt.close('all')
            else:
                plt.show()


    def _match_pointing(self, pointing: NDAPointing) -> Time:
        """ """
        source_mask = pointing.source == self.object
        times_on_source = pointing.time[source_mask]
        time_mask = (times_on_source >= self.time.min()) & (times_on_source <= self.time.max())
        return times_on_source[time_mask]


    @staticmethod
    def _display_pointing(ax, times: Time) -> None:
        """ """
        for time in times:
            ax.axvline(
                time.datetime,
                color='black',
                linestyle='-.',
                alpha=0.5
            )

    @staticmethod
    def _rebin_array(array: np.ndarray, x_step: int = None, y_step: int = None) -> np.ndarray:
        """
            array: 2D array dimensions=(x, y)
            x_step: integer value such that x//x_step = 0 
            y_step: integer value such that y//y_step = 0 
        """

        if x_step is not None:
            x_size_final = array.shape[0]/x_step
            if not x_size_final.is_integer():
                raise ValueError(f'Array x dimension {array.shape[0]} is not divisible by {x_step}.')
            if array.ndim == 2:
                array = np.nanmean(
                    array.reshape((int(x_size_final), x_step, array.shape[1])),
                    axis=1
                )
            elif array.ndim == 1:
                array = np.nanmean(
                    array.reshape((int(x_size_final), x_step)),
                    axis=1
                )
            else:
                raise ValueError(f'Array dimension {array.ndim} is not supported.')

        if y_step is not None:
            if array.ndim != 2:
                raise ValueError('Only 2D arrays.')
            y_size_final = array.shape[1]/y_step
            if not y_size_final.is_integer():
                raise ValueError(f'Array y dimension {array.shape[1]} is not divisible by {y_step}.')
            array = np.nanmean(
                array.reshape((array.shape[0], int(y_size_final), y_step)),
                axis=2
            )

        return array
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# NDA class
# ------------------------------------------------------------- #
class NDA:
    """ Class to handle NDA/NewRoutine/Mefisto data. """


    def __init__(self, file_name: str):
        self.object = None # updated by file_name
        self.receiver = None # updated by file_name
        self.file_name = file_name
        
        self.time = None
        self.frequency = None
        self.accumulation_factor = None
        self.data = None
       
        self._load_data()

        log.info(f"'{self.object.capitalize()}' file")
        log.info(f"Time: {self.time_min.isot} - {self.time_max.isot}")
        log.info(f"Frequency: {self.freq_min} - {self.freq_max}")


    # --------------------------------------------------------- #
    # --------------------- Getter/Setter --------------------- #
    @property
    def file_name(self) -> str:
        """ """
        return self._file_name
    @file_name.setter
    def file_name(self, name: str):
        log.info(f'Reading {name}...')

        # Find out which receiver corresponds to the file
        # for receiver in NDA_RECEIVERS:
        #     if name.endswith(NDA_RECEIVERS[receiver]['file_suffix']):
        #         self.receiver = receiver
        #         break
        # else:
        #     raise ValueError(
        #         f"file_name '{name}' does not end with a known available suffix: {[rec['file_suffix'] for rec in NDA_RECEIVERS.values()]}."
        #     )
        self.receiver = self._parse_receiver_name(name)

        # Find out which source was observed
        first_letter = os.path.basename(name)[0]
        if not first_letter in SOURCE_CODE:
            raise KeyError(
                f"Key '{first_letter}', the first letter of the file '{name}' does not correspond to any known source."
            )
        self.object = SOURCE_CODE[first_letter]

        self._file_name = name


    @property
    def time_min(self) -> Time:
        """ """
        return self.time[0]


    @property
    def time_max(self) -> Time:
        """ """
        return self.time[-1]


    @property
    def freq_min(self) -> u.Quantity:
        """ """
        return self.frequency[0]


    @property
    def freq_max(self) -> u.Quantity:
        """ """
        return self.frequency[-1]


    # --------------------------------------------------------- #
    # ------------------------ Methods ------------------------ #
    def get(self,
            data_type: str = 'LL',
            freq_min: u.Quantity = None,
            freq_max: u.Quantity = None,
            time_min: Time = None,
            time_max: Time = None
        ):
        """ """
        if freq_min is None:
            freq_min = self.frequency[0]
        if freq_max is None:
            freq_max = self.frequency[-1]
        if time_min is None:
            time_min = self.time[0]
        if time_max is None:
            time_max = self.time[-1]
        frequency_mask = (self.frequency >= freq_min) & (self.frequency <= freq_max)
        time_mask = (self.time >= time_min) & (self.time <= time_max)
        try:
            type_to_idx = NDA_RECEIVERS[self.receiver]['source'][self.object.lower()]
        except KeyError:
            type_to_idx = NDA_RECEIVERS[self.receiver]['source']['transit']
        log.info(f'Selecting {data_type} data, frequency: {freq_min} - {freq_max}, time: {time_min.isot} - {time_max.isot}...')
        try:
            return NDASlice(
                time=self.time[time_mask],
                frequency=self.frequency[frequency_mask],
                data=self.data[:, type_to_idx[data_type.lower()], :][time_mask][:, frequency_mask],
                object=self.object
            )
        except KeyError:
            log.error(
                f"data_type: '{data_type}' is invalid. Select one of {list(type_to_idx.keys())}."
            )


    def export_fits(self, save_path: str, pointing: NDAPointing) -> str:
        """ """
        
        # Compute the time-frequency resolution of file
        df = (self.freq_max - self.freq_min)/(self.frequency.size - 1)
        dt = ((self.time[-1] - self.time[0])/(self.time.size - 1)).sec*u.s

        # Initialize the correct FITS writing class
        metadata = {
            'acc': self.accumulation_factor,
            'object': self.object,
            'receiver': self.receiver,
            '_parent_file': os.path.basename(self.file_name)
        }
        if self.object.lower() == 'sun':
            nda_fits = NDASunFits(**metadata, df=df, dt=dt)
        elif self.object.lower() == 'jupiter':    
            nda_fits = NDAJupiterFits(**metadata, df=df, dt=dt)
        else:
            nda_fits = NDATransitFits(**metadata, df=df, dt=dt)

        # Assign the data to the various HDUs
        nda_fits.store_data(
            data=self.data,
            time=self.time,
            frequency=self.frequency,
            pointing=pointing,
        )

        file_pathname = nda_fits.save_file(save_path=save_path)
        #self.fits_filename=nda_fits._file_basename
        return file_pathname


    def plot_quicklooks(self,
            path_name: str = '',
            color: bool = True,
            pdf: bool = False,
            pointing: NDAPointing = None,
            **kwargs) -> str:
        """
            kwargs
                percentile_clip_min
                percentile_clip_max
        """

        # Check the colormap
        cmap = QL_PARAMETERS['color'][color]['cmap']
        color_acc = QL_PARAMETERS['color'][color]['acronym']

        # Check the figname
        if not os.path.isdir(path_name):
            raise FileNotFoundError(f'path_name: {path_name} does not exist.')
        # acronym = NDA_RECEIVERS[self.receiver]['acronym']
        # objabrv = self.object.lower()[:3]
        # start = self.time[0]
        # start.precision = 0 # second precision
        # timeofday = start.isot.replace('-', '').replace('T', '').replace(':', '')
        ql_format = 'pdf' if pdf else 'png'
        # figname = os.path.join(
        #     path_name,
        #     f'NDA_{acronym}_{objabrv}_{timeofday}_QL_{color_acc}.{ql_format}'
        # )
        obj = self.object.lower()[:3]
        rec = self.receiver.lower()
        source = self.object.lower() if self.object.lower() in ['sun', 'jupiter'] else 'transit'
        receiver_key = NDA_RECEIVERS[self.receiver.lower()]['acronym']
        _config_kw = f'{source}_{receiver_key.lower()}_fits_config'
        ver = FITS_KWARGS[_config_kw]['version_spec'] # pynda.__version__.replace(".", "-")
        time_groups = np.arange(1, 6)
        pattern = r"(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2})"
        start = "".join(re.search(pattern, self.time_min.isot).group(*time_groups))
        stop = "".join(re.search(pattern, self.time_max.isot).group(*time_groups))
        figname = os.path.join(
            path_name,
            f"orn_nda_{rec}_{obj}_edr_{start}_{stop}_v{ver}_ql_{color_acc}.{ql_format}"
        )

        # Define the plot structure
        fig, (ax1, ax2) = plt.subplots(2, figsize=(14, 9))
        plt.subplots_adjust(
            right=0.85,
            left=0.1,
            bottom=0.15,
            top=0.85,
            hspace=0.5
        )

        # Select and rebin the data
        obj_name = self.object.lower() if self.object.lower() in ['sun', 'jupiter'] else 'other'
        freq_min = QL_PARAMETERS['frequency'][self.receiver][obj_name]['freq_min']*u.MHz
        freq_max = QL_PARAMETERS['frequency'][self.receiver][obj_name]['freq_max']*u.MHz

        ll_data_slice = self.get(data_type='LL', freq_min=freq_min, freq_max=freq_max)
        rr_data_slice = self.get(data_type='RR', freq_min=freq_min, freq_max=freq_max)

        ll_data_slice = ll_data_slice.time_rebin(n_time_min=QL_PARAMETERS['time_size']['min'], n_time_max=QL_PARAMETERS['time_size']['max'])
        rr_data_slice = rr_data_slice.time_rebin(n_time_min=QL_PARAMETERS['time_size']['min'], n_time_max=QL_PARAMETERS['time_size']['max'])

        smin = kwargs.get('percentile_clip_min', QL_PARAMETERS['thresholds'][self.receiver][obj_name]['smin'])
        smax = kwargs.get('percentile_clip_max', QL_PARAMETERS['thresholds'][self.receiver][obj_name]['smax'])

        # Plot the dynamic spectra
        ll_data_slice.plot_dynamic_spectrum(
            pointing=pointing,
            fig_ax=(fig, ax1),
            decibel=True,
            cbar_label='dB',
            cmap=cmap,
            percentile_clip_min=smin,
            percentile_clip_max=smax,
            title=f"Nançay Decameter Array / {NDA_RECEIVERS[self.receiver]['name']} / LH flux density - {self.object}"
        )
        rr_data_slice.plot_dynamic_spectrum(
            pointing=pointing,
            fig_ax=(fig, ax2),
            decibel=True,
            cbar_label='dB',
            cmap=cmap,
            percentile_clip_min=smin,
            percentile_clip_max=smax,
            title=f"Nançay Decameter Array / {NDA_RECEIVERS[self.receiver]['name']} / RH flux density - {self.object}"
        )

        log.info(f'Saving the quicklook as {figname}...')
        fig.savefig(
            figname,
            dpi=300,
            bbox_inches='tight',
            transparent=False
        )
        plt.close('all')
        return figname


    def clean_data(self):
        """ """
        # TO DO
        return 


    # --------------------------------------------------------- #
    # ----------------------- Internal ------------------------ #
    @staticmethod
    def _parse_receiver_name(file_name: str) -> str:
        """ """
        src_codes_list = "".join(SOURCE_CODE.keys())
        for receiver in NDA_RECEIVERS:
            for suffix in NDA_RECEIVERS[receiver]["file_suffix"]:
                pattern = fr"[{src_codes_list}](_?\d{{8}}_\d{{6}}){{1,2}}{suffix}$"
                re_search = re.search(pattern, file_name)
                if not (re_search is None):
                    return receiver
        else:
            raise ValueError(
                f"File named '{file_name}' is not recognized as either a NewRoutine or Mefisto file."
            )


    @staticmethod
    def _convert_date_to_jd(dates: np.ndarray) -> Time:
        """ """
        log.info('Converting dates to Julian days...')
        jd = np.float64(dates['num_sec'])/np.float64(dates['den_sec'])
        jd +=  np.float64(dates['seconds'])
        jd /= (3600*24)
        jd += np.float64(dates['JDN'])
        return Time(jd, format='jd')


    def _load_data(self) -> None:
        """ Decodes the binary file. """

        if not os.path.isfile(self.file_name):
            raise FileNotFoundError(f"NDA data file '{self.file_name}' not found.")

        # Decode the first part of the header to find out nfreqs
        partial_header_struct = [
            ('size', 'uint32'),
            ('sel_prod0', 'uint32'),
            ('sel_prod1', 'uint32'),
            ('acc', 'uint32'),
            ('subband', 'uint32', (64,)),
            ('nfreq', 'uint32')
        ]
        with open(self.file_name, 'rb') as rf:
            header_dtype = np.dtype(partial_header_struct)
            header = np.frombuffer(
                rf.read(header_dtype.itemsize),
                count=1,
                dtype=header_dtype,
            )[0]
        n_frequencies = header['nfreq']

        # Decode the second part of the header
        header_struct = partial_header_struct + [
            ('freq', 'float32', (n_frequencies,)),
            ('ifrq', 'int32', (n_frequencies,))
        ]
        with open(self.file_name, 'rb') as rf:
            header_dtype = np.dtype(header_struct)
            header = np.frombuffer(
                rf.read(header_dtype.itemsize),
                count=1,
                dtype=header_dtype,
            )[0]

        # Decode the data
        sel_prod_0 = np.binary_repr(header['sel_prod0'], 32).count('1')
        sel_prod_1 = np.binary_repr(header['sel_prod1'], 32).count('1')
        n_prod = sel_prod_0 + sel_prod_1
        spectrum_dtype = np.dtype([
            ('magic', 'float32'),
            ('no', 'uint32'),
            ('data', 'float32', (n_frequencies,))
        ])
        time_dtype = np.dtype([
            ('JDN', 'uint32'),
            ('seconds', 'uint32'),
            ('num_sec', 'uint32'),
            ('den_sec', 'uint32')
        ])
        cube_dtype = np.dtype([
            ('magic', 'float32'),
            ('id', 'uint32'),
            ('date', time_dtype),
            ('length', 'uint32'),
            ('status', 'uint32'),
            ('corr', spectrum_dtype, (n_prod))
        ])
        with open(self.file_name, 'rb') as rf:
            # Construct a memory mapping
            tmp = np.memmap(
                rf,
                dtype='float32',
                offset=header['size'],
                mode='r'
            )
        log.info(f'Successfully decoded {self.file_name}.')

        # Parse the data
        cube = tmp[:tmp.size].view(cube_dtype)
        self.data = da.from_array(cube['corr']['data']) # xx, re_xy, im_xy, yy
        self.accumulation_factor = header['acc']
        self.data /= self.accumulation_factor
        log.info(
            f'Data divided by the accumulation factor {self.accumulation_factor}.'
        )
        # Convert and store the time in attribute
        self.time = self._convert_date_to_jd(cube['date'])
        # Store the frequency
        self.frequency = header['freq']*u.MHz
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# NDA Fits reading class
# ------------------------------------------------------------- #
class NDAFits:
    """ Class to read NDA in FITS format. """

    def __init__(self, file_name: str):
        self.object = None
        self._channels = None
        self.file_name = file_name

        # Read the fits file
        log.info('Parsing the data...')
        with fits.open(self.file_name) as hdus:
            self.frequency = np.squeeze(hdus['SETUP'].data['frq'])*u.MHz
            self.time = Time(hdus['SCIENCE'].data['jd'], format='jd')
            self.data = da.from_array(np.swapaxes(hdus['SCIENCE'].data['data'], 1, 2))

        log.info(f"'{self.object.capitalize()}' file")
        log.info(f"Time: {self.time[0].isot} - {self.time[-1].isot}")
        log.info(f"Frequency: {self.frequency[0]} - {self.frequency[-1]}")

    # --------------------------------------------------------- #
    # --------------------- Getter/Setter --------------------- #
    @property
    def file_name(self) -> str:
        return self._file_name
    @file_name.setter
    def file_name(self, name) -> None:
        # Check that the file exists
        if not os.path.isfile(name):
            raise FileNotFoundError(f'Unable to find {name}.')

        # Check that this is a valid NDA Fits file
        try:
            header = fits.getheader(name)
        except OSError:
            log.error(f'File {name} is not recognized as a valid FITS file.')
            raise
        if (header['TELESCOP'] == 'NDA') & (header['INSTRUME'] in ['newroutine', 'mefisto']):
            self.object = header['OBJECT']
            self._channels = self._get_available_channels(header)
        else:
            raise Exception(f'File {name} does not look like a NDA NewRoutine or MEFISTO file.')

        # Check that the version is correct
        supported_version = '>=0.3.0'
        vspec = SpecifierSet(supported_version)
        if not header['SFW-VERS'] in vspec:
            raise Exception(f"The file {name} was created with pynda, version {header['SFW-VERS']}. However only version {supported_version} are supported.")

        log.info(f'Reading {name}...')

        self._file_name = name


    # --------------------------------------------------------- #
    # ------------------------ Methods ------------------------ #
    def get(self,
            data_type: str = 'LL',
            freq_min: u.Quantity = None,
            freq_max: u.Quantity = None,
            time_min: Time = None,
            time_max: Time = None
        ):
        """ """
        if freq_min is None:
            freq_min = self.frequency[0]
        if freq_max is None:
            freq_max = self.frequency[-1]
        if time_min is None:
            time_min = self.time[0]
        if time_max is None:
            time_max = self.time[-1]
        frequency_mask = (self.frequency >= freq_min) & (self.frequency <= freq_max)
        time_mask = (self.time >= time_min) & (self.time <= time_max)
        type_to_idx = self._channels
        log.info(f'Selecting {data_type} data, frequency: {freq_min} - {freq_max}, time: {time_min.isot} - {time_max.isot}...')
        try:
            return NDASlice(
                time=self.time[time_mask],
                frequency=self.frequency[frequency_mask],
                data=self.data[:, type_to_idx[data_type.lower()], :][time_mask][:, frequency_mask],
                object=self.object
            )
        except KeyError:
            log.error(
                f"data_type: '{data_type}' is invalid. Select one of {list(type_to_idx.keys())}."
            )

    # --------------------------------------------------------- #
    # ----------------------- Internal ------------------------ #
    @staticmethod
    def _get_available_channels(header: dict) -> dict:
        """ """
        channels = {}
        for i in range(1, 5):
            try:
                channels[header[f'CHANNEL{i}'].lower()] = i - 1
            except KeyError:
                continue
        return channels
# ------------------------------------------------------------- #
# ============================================================= #

