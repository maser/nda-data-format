#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    NewRoutine/Mefisto Fits files
"""


__author__ = 'Alan Loh'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'NDABaseFits'
    'NDAJupiterFits',
    'NDASunFits',
    'NDATransitFits'
]


from abc import ABC, abstractmethod
import numpy as np
import re
import os
from astropy.time import Time
from astropy.io import fits
import json
import astropy.units as u

import pynda
from pynda import NDA_FITS_DATA_COLUMNS, NDA_RECEIVERS
from pynda.pointing import NDAPointing
from pynda.ephemeris.ephem import Jupiter, JupiterMoon, Sun

import logging
log = logging.getLogger(__name__)


# read the FITS keywords from the JSON file
with open(os.path.join(os.path.dirname(__file__), 'newroutine_fits_keywords.json'), 'r') as keyword_file:
    FITS_KWARGS = json.load(keyword_file)


JOVIAN_RADIUS = 69911000 # jovian radius in m


# ============================================================= #
# NDA Fits general class
# ------------------------------------------------------------- #
def _check_hdu_header(hdu: fits.BinTableHDU, header_name: str) -> None:
    """ Check the conformity of a HDU header against what is defined in the JSON file. """
    setup_metadata = FITS_KWARGS[header_name]
    for key in setup_metadata.keys():
        if not hdu.header[key] == setup_metadata[key]['value']:
            if setup_metadata[key]['value'] is None:
                continue
            log.warning(f"Metadata of {hdu.name} do not match JSON specification: '{key}'={hdu.header[key]} instead of {setup_metadata[key]['value']}.")


class NDABaseFits(ABC):
    """ """


    def __init__(self, **kwargs):
        # Set attributes from general metadata
        self._set_attr_from_kwargs(**kwargs)

        # Initialize the list of HDUs from which the FITS will be constructed
        self.hdu_list = []


    # --------------------------------------------------------- #
    # --------------------- Getter/Setter --------------------- #
    @property
    @abstractmethod
    def _exception_metadata_list(self):
        pass


    @property
    def _freq_min(self) -> u.Quantity:
        """ Gets the minimal frequency from the configuration. """
        f_min = FITS_KWARGS[self._config_kw]['freq_min'] * u.MHz
        log.info(
            f"Minimal frequency to include: {f_min}."
        )
        return f_min


    @property
    def _freq_max(self) -> u.Quantity:
        """ Gets the maximal frequency from the configuration. """
        f_max = FITS_KWARGS[self._config_kw]['freq_max'] * u.MHz
        log.info(
            f"Maximal frequency to include: {f_max}."
        )
        return f_max


    @property
    def _file_basename(self) -> str:
        """ File name constructed from the different attributes. 
            It needs to look like orn_nda_newroutine_jup_edr_YYYYMMDDHHMM_YYYYMMDDHHMM_V1.0.fits
        """
        obj = self.object.lower()[:3]
        rec = self.receiver.lower()
        ver = self._data_specification_version # pynda.__version__.replace(".", "-")
        time_groups = np.arange(1, 6)
        pattern = r"(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2})"
        start = "".join(re.search(pattern, self._tmin.isot).group(*time_groups))
        stop = "".join(re.search(pattern, self._tmax.isot).group(*time_groups))
        return f"orn_nda_{rec}_{obj}_edr_{start}_{stop}_v{ver}"


    @property
    def _config_kw(self):
        source = self.object.lower() if self.object.lower() in ['sun', 'jupiter'] else 'transit'
        receiver_key = NDA_RECEIVERS[self.receiver.lower()]['acronym']
        return f'{source}_{receiver_key.lower()}_fits_config'


    @property
    def _data_specification_version(self) -> str:
        """ Returns the data specification version, hardcoded in the JSON file. """
        return FITS_KWARGS[self._config_kw]['version_spec']


    # --------------------------------------------------------- #
    # ------------------------ Methods ------------------------ #
    @classmethod
    @abstractmethod
    def from_fits(cls):
        return NotImplementedError


    def store_data(self,
            data: dict,
            time: Time,
            frequency: u.Quantity,
            pointing: NDAPointing = None,
        ) -> None:
        """ """
        self.hdu_list.append(
            self._set_primary_hdu(time)
        )

        self.hdu_list.append(
            self._set_setup_hdu(frequency)
        )

        self.hdu_list.append(
            self._set_data_hdu(time, data)
        )

        if pointing is not None:
            self.hdu_list.append(
                self._set_tracking_hdu(pointing)
            )


    def save_file(self, save_path: str) -> str:
        """ """
        file_name = os.path.join(save_path, self._file_basename + ".fits")

        log.info(f"Saving {file_name}...")
        fits.HDUList(self.hdu_list).writeto(file_name, overwrite=True)
        log.info(f"File {file_name} saved.")
        return file_name


    # --------------------------------------------------------- #
    # ----------------------- Internal ------------------------ #
    def _set_attr_from_kwargs(self, **kwargs):
        """ Sets attributes corresponding to the keys listed in the keyword
            JSON file. Raises an error if one of the key has not been set.
        """
        metadata = FITS_KWARGS['general_metadata']
        for key in metadata.keys():
            try:
                setattr(self, key, kwargs[key])
            except KeyError:
                log.error(
                    f"Key '{key}' expected while initializing {type(self)}."
                )
                raise


    @abstractmethod
    def _get_ephemeris_dict(time: Time) -> dict:
        """ """
        raise NotImplementedError("Write a '_get_ephemeris_dict' method first!")


    @staticmethod
    def _prepare_time_webgeocalc(time: Time, n_points: int = 200) -> Time:
        # Sample the time range to speed things up and because webgeocalc only supports 10k data points
        if not time.isscalar:
            dt = (time[-1] - time[0])/(n_points - 1)
            time = time[0] + np.arange(n_points)*dt
        return time


    def _set_primary_hdu(self, time: Time) -> fits.PrimaryHDU:
        """ """
        log.info("Setting up 'primary' HDU...")

        # Define header-specific metadata
        self._tmin = time[0]
        self._tmax = time[-1]
        start_date, start_time = self._tmin.isot.split('T')
        end_date, end_time = self._tmax.isot.split('T')
        freq_min = self.freq_min.round(2)
        freq_max = self.freq_max.round(2)

        # Preparing the metadata dictionnary
        config_kwargs = FITS_KWARGS[self._config_kw]
        metadata = {
            'TITLE': f'ORN NDA {self.receiver} {self.object.upper()} EDR Dataset',
            'DATE': Time.now().isot.split('T')[0],
            'DATE-BEG': start_date,
            'DATE-END': end_date,
            'TSTART': start_time,
            'TSTOP': end_time,
            'OBJECT': self.object,
            'ACC': self.acc,
            'INSTRUME': self.receiver,
            'SUBJECT': config_kwargs['subject'],
            'FILENAME': self._file_basename,
            'VERSION': self._data_specification_version,
            'FREQMIN': freq_min.to(u.MHz).value,
            'FREQMAX': freq_max.to(u.MHz).value,
            'DSCPLN': config_kwargs['discipline'],
            'DSCRPTR': f"{config_kwargs['receiver_name']}_{self.object}",
            'PARENTS': self._parent_file,
            'SFW-VERS': pynda.__version__,
            'OBJ-CLAS': config_kwargs['targetclass'],
            'OBJ-REGN': config_kwargs['targetregion'],
            'FEA-NAME': config_kwargs['featurename'],
            'DF': self.df.to(u.MHz).value,
            'DT': self.dt.to(u.s).value,
            'REFDATA': config_kwargs['doi_data'],
            'REFSPEC': config_kwargs['doi_spec']
        }
        updated_metadata = {**metadata, **self._get_ephemeris_dict(time)}
        updated_metadata_keys = np.array(list(updated_metadata.keys()))

        primary_hdu = fits.PrimaryHDU()
        header = primary_hdu.header

        # Available metadata for this HDU
        primary_hdu_kwargs = FITS_KWARGS['primary_header']
        virtual_obs_kwargs = FITS_KWARGS['virtual_obs']
        source = self.object.lower()
        ephemeris_kwargs = FITS_KWARGS[f"{source if source in ['sun', 'jupiter'] else 'transit'}_ephemeris"]

        # Set the header key to the metadata dict value if it exists
        metadata_dict = {**primary_hdu_kwargs, **virtual_obs_kwargs, **ephemeris_kwargs}

        # Check that every updated metadata field is included in the JSON file with its description and comment
        expected_keys = np.array(list(metadata_dict.keys()))
        # Raise warnings if metadata keys are not in the JSON file
        unexpected_metadata = ~np.isin(updated_metadata_keys, expected_keys)
        if np.any(unexpected_metadata):
            log.warning(f"Unexpected metadata keys: {updated_metadata_keys[unexpected_metadata]}!")

        # For every available metadata field
        for key, entry in metadata_dict.items():
            if key in self._exception_metadata_list:
                # Skip this metadata
                continue

            entry_value = entry['value']
            entry_comment = entry['comment']

            # Check if there is an updated value and update accordingly
            if entry_value is None:
                # It has to be updated
                if key in updated_metadata:
                    entry_value = updated_metadata[key]
                else:
                    log.warning(f"Key '{key}' requires to be updated by the conversion script!")
            elif key in updated_metadata:
                # It should not be updated?
                # Do it anyway but log a warning...
                entry_value = updated_metadata[key]
                log.warning(f"Key '{key}' does not require to be updated. But the conversion script knows better...")

            header.append((key, entry_value, entry_comment))

        # Check the new values entered in the header attribute        
        primary_hdu.verify()

        return primary_hdu


    def _set_setup_hdu(self, frequency: u.Quantity) -> fits.BinTableHDU:
        """ """
        log.info("Setting up 'setup' HDU...")

        # Select the frequencies based on the configuration
        f_min = self.freq_min
        f_max = self.freq_max
        self._frequency_mask = (frequency >= f_min) & (frequency <= f_max)
        selected_frequencies = frequency[self._frequency_mask]
        number_of_freqs = selected_frequencies.size
        frequency_format = f'{number_of_freqs}E'

        # Construct the columns
        # FITS format https://docs.astropy.org/en/stable/io/fits/usage/table.html#column-creation
        c1 = fits.Column(
            name='frq',
            array=selected_frequencies.to(u.MHz).value.reshape((1, number_of_freqs)),
            format=frequency_format,
            unit='MHz',
        )

        hdu = fits.BinTableHDU.from_columns(
            columns=[c1],
            nrows=1,
            name='SETUP'
        )

        _check_hdu_header(hdu=hdu, header_name='setup_header')

        return hdu


    @staticmethod
    def _set_tracking_hdu(pointing: NDAPointing) -> fits.BinTableHDU:
        """ """
        log.info("Setting up 'acquisition' HDU...")

        # Construct the columns
        # FITS format https://docs.astropy.org/en/stable/io/fits/usage/table.html#column-creation
        c1 = fits.Column(
            name='jd',
            array=pointing.time.jd,
            format='1D',
            unit='d'
        )
        altaz = pointing.altaz
        c2 = fits.Column(
            name='azimuth',
            array=altaz.az.deg,
            format='1E',
            unit='deg',
        )
        c3 = fits.Column(
            name='altitude',
            array=altaz.alt.deg,
            format='1E',
            unit='deg'
        )
        eq = pointing.coordinates
        c4 = fits.Column(
            name='right_ascension',
            array=eq.ra.deg,
            format='1E',
            unit='deg'
        )
        c5 = fits.Column(
            name='declination',
            array=eq.dec.deg,
            format='1E',
            unit='deg'
        )
        c6 = fits.Column(
            name='at',
            array=pointing.attenuation,
            format='1I',
            unit='dB'
        )
        c7 = fits.Column(
            name='filter_min_field',
            array=pointing.field_frequency_min,
            format='1I',
            unit='MHz'
        )
        c8 = fits.Column(
            name='filter_max_field',
            array=pointing.field_frequency_max,
            format='1I',
            unit='MHz'
        )
        c9 = fits.Column(
            name='filter_lab',
            array=pointing.lab_frequency,
            format='1I',
            unit='MHz'
        )

        hdu = fits.BinTableHDU.from_columns(
            columns=[c1, c2, c3, c4, c5, c6, c7, c8, c9],
            name='ACQUISITION'
        )

        _check_hdu_header(hdu=hdu, header_name='tracking_header')

        return hdu


    def _set_data_hdu(self, time: Time, data: np.ndarray) -> fits.BinTableHDU:
        """ """
        log.info("Setting up 'science' HDU...")

        c1 = fits.Column(
            name='jd',
            array=time.jd,
            format='1D',
            unit='d'
        )

        # Select polarization columns depending on receiver and source observed
        source = self.object.lower() if self.object.lower() in ['sun', 'jupiter'] else 'transit'
        data = data[:, NDA_FITS_DATA_COLUMNS[self.receiver.lower()][source], :]

        data_to_store = np.swapaxes(
            data[:, :, self._frequency_mask],
            1, 2
        )
        data_format = f'{np.prod(data_to_store.shape[1:])}E'
        data_dimension = f'({data_to_store.shape[2]}, {data_to_store.shape[1]})'

        c2 = fits.Column(
            name='data',
            array=data_to_store,
            dim=data_dimension,
            format=data_format,
            unit='V**2/Hz'
        )

        hdu = fits.BinTableHDU.from_columns(
            columns=[c1, c2],
            name='SCIENCE',
            nrows=data_to_store.shape[0]
        )

        _check_hdu_header(hdu=hdu, header_name='datacube_header')

        return hdu
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Jupiter FITS class
# ------------------------------------------------------------- #
class NDAJupiterFits(NDABaseFits):
    """ """

    _exception_metadata_list = []

    def __init__(self, df: u.Quantity, dt: u.Quantity,  **kwargs):
        super().__init__(**kwargs)
        log.info(f'Initializing Jupiter FITS class.')
        self.freq_min = self._freq_min
        self.freq_max = self._freq_max
        self.df = df
        self.dt = dt


    @classmethod
    def from_fits(cls, file_name: str):
        """ """
        log.info(f'Reading {file_name}...')
        raise NotImplementedError('To be done.')


    def _get_ephemeris_dict(self, time: Time) -> dict:
        """ """

        time = self._prepare_time_webgeocalc(time)

        # Prepare the dictionnary of ephemeris seen from the Earth
        jupiter = Jupiter.seen_from('Earth', time)
        meridian_transit = jupiter.next_meridian_transit().isot
        coordinates = jupiter.get_sky_coordinates()
        earth_distance = jupiter.target_distance.to(u.AU)
        jupiter_cml = jupiter.cml
        earth_lat = jupiter.target_latitude
        earth_ephemeris_dict = {
            'DATE-MER': meridian_transit.split('T')[0],
            'TIME-MER': meridian_transit.split('T')[1],
            'DECOBJ-B': coordinates[0].dec.deg,
            'DECOBJ-E': coordinates[-1].dec.deg,
            'RAOBJ-B': coordinates[0].ra.deg,
            'RAOBJ-E': coordinates[-1].ra.deg,
            'SUBELATB': earth_lat[0].deg,
            'SUBELATE': earth_lat[-1].deg,
            'SUBELAT1': np.min(earth_lat).deg,
            'SUBELAT2': np.max(earth_lat).deg,
            'DISTE-B': earth_distance[0].value,
            'DISTE-E': earth_distance[-1].value,
            'DISTE-1': np.min(earth_distance).value,
            'DISTE-2': np.max(earth_distance).value,
            'SUBELONB': jupiter_cml[0].deg,
            'SUBELONE': jupiter_cml[-1].deg,
        }

        # Prepare the dictionnary of ephemeris seen from the Sun
        sun_E = Sun.seen_from('Earth', time)
        sun_J = Sun.seen_from('Jupiter', time)
        sun_distance = sun_J.target_distance.to(u.AU)
        sun_lat = sun_E.target_latitude
        sun_long = sun_E.target_longitude
        carr_rot = Sun.carrington_rotation(time)
        sun_ephemeris_dict = {
            'DISTS-B': sun_distance[0].value,
            'DISTS-E': sun_distance[-1].value,
            'DISTS-1': np.min(sun_distance).value,
            'DISTS-2': np.max(sun_distance).value,
            'SUBSLATB': sun_lat[0].deg,
            'SUBSLATE': sun_lat[-1].deg,
            'SUBSLAT1': np.min(sun_lat).deg,
            'SUBSLAT2': np.max(sun_lat).deg,
            'SUBSLONB': sun_long[0].deg,
            'SUBSLONE': sun_long[-1].deg,
            'CARROT-B': np.min(carr_rot),
            'CARROT-E': np.max(carr_rot),
        }

        # # Construct an ephemeris dictionnary out of that
        moon_ephemeris_dict = {}
        
        # for prefix, ephem in zip(moons_preffix, moon_ephems):
        for moon in JupiterMoon:
            prefix = moon.value[:2]
            ephem = moon.seen_from('EARTH', time)
            ephem_dict = {
                f'LT_{prefix}-B': ephem.target_local_time[0].hour,
                f'LT_{prefix}-E': ephem.target_local_time[-1].hour,
                f'LT_{prefix}-1': np.min(ephem.target_local_time).hour,
                f'LT_{prefix}-2': np.max(ephem.target_local_time).hour,
                f'LON_{prefix}-B': ephem.target_longitude[0].deg,
                f'LON_{prefix}-E': ephem.target_longitude[-1].deg,
                f'MLAT{prefix}-B': ephem.target_mag_latitude[0].deg,
                f'MLAT{prefix}-E': ephem.target_mag_latitude[-1].deg,
                f'MLAT{prefix}-1': np.min(ephem.target_mag_latitude).deg,
                f'MLAT{prefix}-2': np.max(ephem.target_mag_latitude).deg,
                f'RJ_{prefix}-B': ephem.target_distance[0].to(u.m).value/JOVIAN_RADIUS,
                f'RJ_{prefix}-E': ephem.target_distance[-1].to(u.m).value/JOVIAN_RADIUS,
                f'RJ_{prefix}-1': np.min(ephem.target_distance).to(u.m).value/JOVIAN_RADIUS,
                f'RJ_{prefix}-2': np.max(ephem.target_distance).to(u.m).value/JOVIAN_RADIUS,
            }
            # Update the current dictionnary
            moon_ephemeris_dict.update(ephem_dict)

        # Return the combination of dictionnaries
        return {**earth_ephemeris_dict, **moon_ephemeris_dict, **sun_ephemeris_dict}
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Sun FITS class
# ------------------------------------------------------------- #
class NDASunFits(NDABaseFits):
    """ """

    _exception_metadata_list = ['CHANNEL3', 'CHANNEL4']

    def __init__(self, df: u.Quantity, dt: u.Quantity, **kwargs):
        super().__init__(**kwargs)
        log.info(f'Initializing Sun FITS class.')
        self.freq_min = self._freq_min
        self.freq_max = self._freq_max
        self.df = df
        self.dt = dt


    @classmethod
    def from_fits(cls, file_name: str):
        """ """
        log.info(f'Reading {file_name}...')
        raise NotImplementedError('To be done.')


    def _get_ephemeris_dict(self, time: Time) -> dict:
        """ """
        time = self._prepare_time_webgeocalc(time)

        # Prepare the dictionnary of ephemeris seen from the Earth
        sun = Sun.seen_from('Earth', time)
        meridian_transit = sun.next_meridian_transit().isot
        coordinates = sun.get_sky_coordinates()
        earth_distance = sun.target_distance.to(u.AU)
        jupiter_cml = sun.cml
        earth_lat = sun.target_latitude
        carr_rot = Sun.carrington_rotation(time)
        earth_ephemeris_dict = {
            'DATE-MER': meridian_transit.split('T')[0],
            'TIME-MER': meridian_transit.split('T')[1],
            'DECOBJ-B': coordinates[0].dec.deg,
            'DECOBJ-E': coordinates[-1].dec.deg,
            'RAOBJ-B': coordinates[0].ra.deg,
            'RAOBJ-E': coordinates[-1].ra.deg,
            'SUBELATB': earth_lat[0].deg,
            'SUBELATE': earth_lat[-1].deg,
            'SUBELAT1': np.min(earth_lat).deg,
            'SUBELAT2': np.max(earth_lat).deg,
            'DISTE-B': earth_distance[0].value,
            'DISTE-E': earth_distance[-1].value,
            'DISTE-1': np.min(earth_distance).value,
            'DISTE-2': np.max(earth_distance).value,
            'SUBELONB': jupiter_cml[0].deg,
            'SUBELONE': jupiter_cml[-1].deg,
            'CARROT-B': np.min(carr_rot),
            'CARROT-E': np.max(carr_rot),
        }

        # Return the combination of dictionnaries
        return {**earth_ephemeris_dict}
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Transit FITS class
# ------------------------------------------------------------- #
class NDATransitFits(NDABaseFits):
    """ """

    _exception_metadata_list = ['CHANNEL3', 'CHANNEL4']

    def __init__(self, df: u.Quantity, dt: u.Quantity,**kwargs):
        super().__init__(**kwargs)
        log.info(f'Initializing Transit FITS class.')
        self.freq_min = self._freq_min
        self.freq_max = self._freq_max
        self.df = df
        self.dt = dt


    @classmethod
    def from_fits(cls, file_name: str):
        """ """
        log.info(f'Reading {file_name}...')
        raise NotImplementedError('To be done.')


    def _get_ephemeris_dict(self, time: Time) -> dict:
        """ """
        time = self._prepare_time_webgeocalc(time)

        # Prepare the dictionnary of ephemeris seen from the Earth
        sun = Sun.seen_from('Earth', time)
        meridian_transit = sun.next_meridian_transit().isot
        coordinates = sun.get_sky_coordinates()
        earth_distance = sun.target_distance.to(u.AU)
        jupiter_cml = sun.cml
        earth_lat = sun.target_latitude
        carr_rot = Sun.carrington_rotation(time)
        earth_ephemeris_dict = {
            'DATE-MER': meridian_transit.split("T")[0],
            'TIME-MER': meridian_transit.split("T")[1],
            'DECOBJ-B': coordinates[0].dec.deg,
            'DECOBJ-E': coordinates[-1].dec.deg,
            'RAOBJ-B': coordinates[0].ra.deg,
            'RAOBJ-E': coordinates[-1].ra.deg,
            'SUBELATB': earth_lat[0].deg,
            'SUBELATE': earth_lat[-1].deg,
            'SUBELAT1': np.min(earth_lat).deg,
            'SUBELAT2': np.max(earth_lat).deg,
            'DISTE-B': earth_distance[0].value,
            'DISTE-E': earth_distance[-1].value,
            'DISTE-1': np.min(earth_distance).value,
            'DISTE-2': np.max(earth_distance).value,
            'SUBELONB': jupiter_cml[0].deg,
            'SUBELONE': jupiter_cml[-1].deg,
            'CARROT-B': np.min(carr_rot),
            'CARROT-E': np.max(carr_rot),
        }

        # Return the combination of dictionnaries
        return { **earth_ephemeris_dict}
# ------------------------------------------------------------- #
# ============================================================= #

