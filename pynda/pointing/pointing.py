#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Reading NDA pointing files
"""


__author__ = 'Alan Loh'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'NDAPointing'
]


import numpy as np
import os
from astropy.time import Time
import astropy.units as u
from astropy.coordinates import SkyCoord

import logging
log = logging.getLogger(__name__)

from pynda import (
    LAST_POINTING_FREQUENCY,
    FIELD_FILTERS,
    LAB_FILTER_INDICES,
    LAB_FILTERS
)
from pynda.pointing.pointing_astro import order_to_radec, radec_to_altaz
from pynda.pointing.pointing_file import parse_pointing_file, find_pointing_files


# ============================================================= #
# NDAPointing class
# ------------------------------------------------------------- #
class NDAPointing:


    def __init__(self,
        source: np.ndarray,
        time: Time,
        attenuation: np.ndarray,
        opt_frequency: u.Quantity,
        lab_frequency: u.Quantity,
        field_frequency_min: u.Quantity,
        field_frequency_max: u.Quantity,
        rir: np.ndarray,
        rns: np.ndarray
    ):
        self._file_name = []

        self.source = source
        self.time = time
        self.attenuation = attenuation
        self.opt_frequency = opt_frequency
        self.lab_frequency = lab_frequency
        self.field_frequency_min = field_frequency_min
        self.field_frequency_max = field_frequency_max
        self.rir = rir
        self.rns = rns
        self._coordinates = None


    def __add__(self, other):
        """ Concatenates two NDAPointing objects. """
        
        if other.__class__ != self.__class__:
            raise TypeError(
                f'Concatenation is only possible with another NDAPointing object. Got {type(other)} instead...'
            )

        # Check if there is no common time stamps, that should not be possible
        if np.any(np.isin(self.time.jd, other.time.jd)):
            raise ValueError(
                f"'{self._file_name}' and '{other._file_name}' have common timestamps. That should not be possible."
            )
        # Check that the two objects are not empty, otherwise return only one of them
        elif (self.time.size == 0) and (other.time.size != 0):
            return other
        elif (self.time.size != 0) and (other.time.size == 0):
            return self
        elif (self.time.size == 0) and (other.time.size == 0):
            log.warning('Concatenating two empty NDAPointing objects results in None.')
            return None
        
        log.info(f"Concatenating '{self._file_name}' and '{other._file_name}'...")

        # Define the order of concatenation depending on the time
        if self.time[0] < other.time[0]:
            timesort = lambda x: np.concatenate(x) # do not reverse
        else:
            timesort = lambda x: np.concatenate(x[::-1]) # reverse

        concatenated_file = NDAPointing(
            source=timesort([self.source, other.source]),
            time=Time(timesort([self.time.jd, other.time.jd]), format='jd'),
            attenuation=timesort([self.attenuation, other.attenuation]),
            opt_frequency=timesort([self.opt_frequency, other.opt_frequency]),
            lab_frequency=timesort([self.lab_frequency, other.lab_frequency]),
            field_frequency_min=timesort([self.field_frequency_min, other.field_frequency_min]),
            field_frequency_max=timesort([self.field_frequency_max, other.field_frequency_max]),
            rir=timesort([self.rir, other.rir]),
            rns=timesort([self.rns, other.rns])
        )
        concatenated_file._file_name += self._file_name if isinstance(self._file_name, list) else [self._file_name]
        concatenated_file._file_name += other._file_name if isinstance(other._file_name, list) else [other._file_name]
        return concatenated_file


    # --------------------------------------------------------- #
    # --------------------- Getter/Setter --------------------- #
    @property
    def altaz(self) -> SkyCoord:
        """ Converts pointing orders to azimuth/elevation coordinates. """
        return radec_to_altaz(
            radec=self.coordinates,
            time=self.time
        )

    @property
    def coordinates(self) -> SkyCoord:
        if self._coordinates is None:
            # Do the computation if it has not been done before
            self._coordinates = order_to_radec(
                rir=self.rir,
                rns=self.rns,
                time=self.time
            )
        return self._coordinates


    @property
    def size(self) -> int:
        return self.time.size


    # --------------------------------------------------------- #
    # ------------------------ Methods ------------------------ #
    @classmethod
    def from_file(cls,
            file_name: str,
            time_min: Time = None,
            time_max: Time = None,
            receiver: str = 'newroutine'
        ):
        """ Constructs a NDAPointing object from a NDA pointing file.

            :param file_name:
                Absolute path name to the pointing file.
            :type file_name: `str`
            :param time_min:
                If set, the NDAPointing object will only contain
                pointing timestamps greater or equal to this value.
                Default is ``None``.
            :type time_min: :class:`~astropy.time.Time`
            :param time_max:
                If set, the NDAPointing object will only contain
                pointing timestamps lower or equal to this value.
                Default is ``None``.
            :type time_max: :class:`~astropy.time.Time`
            :param receiver:
                Select the desired NDA receiver.
                This keyword only affects the *lab filter frequency*.
                Default is ``'newroutine'``.
            :type receiver: `str`
        """

        if not os.path.exists(file_name):
            raise FileNotFoundError(f"Unable to find pointing file '{file_name}'.")

        receiver_index = cls._get_lab_filter_index(receiver)
        pointing, sources, opt_freq, date = parse_pointing_file(file_name=file_name)
        pointing_time = Time(list(map(lambda x: date + 'T' + x, pointing['hour'])))
        opt_freqs = cls._set_opt_freq(opt_freq=opt_freq, source=sources, pointing=pointing)

        # Select time interval within this file
        t_mask = cls._compute_time_mask(timestamps=pointing_time, time_min=time_min, time_max=time_max)
        sources = sources[t_mask]
        pointing_time = pointing_time[t_mask]
        pointing = pointing[t_mask]

        pointing_object = cls(
            source=sources,
            time=pointing_time,
            attenuation=cls._set_attenuation(attenuation=pointing['at_right']),
            opt_frequency=opt_freqs[t_mask],
            lab_frequency=[LAB_FILTERS[str(lab_filter)] for lab_filter in pointing[f'f{receiver_index}_right']]*u.MHz,
            field_frequency_min=[FIELD_FILTERS[str(field_filter)][0] for field_filter in pointing['f1_right']]*u.MHz,
            field_frequency_max=[FIELD_FILTERS[str(field_filter)][1] for field_filter in pointing['f1_right']]*u.MHz,
            rir=pointing['rir'],
            rns=pointing['rns_left']
        )
        pointing_object._file_name = [file_name]
        return pointing_object


    @classmethod
    def from_dir(cls,
            dir_path: str,
            time_min: Time,
            time_max: Time,
            receiver: str = 'newroutine',
            file_type: str = 'log_file'
        ):
        """ """

        pointing_files = find_pointing_files(
            dir_path=dir_path,
            file_type=file_type,
            time_min=time_min,
            time_max=time_max
        )

        pointing_object = cls.from_file(
            file_name=pointing_files[0],
            time_min=time_min,
            time_max=time_max,
            receiver=receiver
        )
        for pointing_file in pointing_files[1:]:
            pointing_object += cls.from_file(
                file_name=pointing_file,
                time_min=time_min,
                time_max=time_max,
                receiver=receiver
            )
        return pointing_object


    # --------------------------------------------------------- #
    # ----------------------- Internal ------------------------ #
    @staticmethod
    def _set_opt_freq(opt_freq: u.Quantity, source: np.ndarray, pointing: np.ndarray) -> u.Quantity:
        """ The last pointing stamp is made at the default frequency. """
        
        # Transform the optimization frequency into an array of same size than the pointing
        opt_frequency = opt_freq.repeat(source.size)
        
        # Change the value to the last frequency whenever a final pointing is detected
        # _, change_object_idx = np.unique(source, return_index=True)
        # change_object_idx = np.roll(change_object_idx, -1) - 1
        # opt_frequency[change_object_idx] = LAST_POINTING_FREQUENCY
        default_pointing = {
            'phase_right': 335500,
            'rew_right': 63,
            'rns_right': 64,
            'f1_right': 4,
            'f2_right': 4,
            'f3_right': 4,
            'f4_right': 4,
            'f5_right': 4
        }
        default_pointing_mask = np.all(
            [
                pointing[key] == default_pointing[key] for key in default_pointing.keys()
            ],
            axis=0
        )
        opt_frequency[default_pointing_mask] = LAST_POINTING_FREQUENCY

        return opt_frequency


    @staticmethod
    def _set_attenuation(attenuation: np.ndarray) -> np.ndarray:
        attenuation[attenuation=='OF'] = -1
        return attenuation.astype(int)


    @staticmethod
    def _get_lab_filter_index(receiver: str) -> int:
        if receiver not in LAB_FILTER_INDICES:
            return ValueError(
                f"Receiver '{receiver}' unknown, select one from {LAB_FILTER_INDICES.keys()}."
            )
        return LAB_FILTER_INDICES[receiver]


    @staticmethod
    def _compute_time_mask(timestamps: Time, time_min: Time, time_max: Time) -> np.ndarray:
        if time_min is None:
            time_min = timestamps.min()
        if time_max is None:
            time_max = timestamps.max()

        # Make sure the time precision is identical
        timestamps.precision = 3
        time_min.precision = 3
        time_max.precision = 3

        # Compare Julian days, this is more precise to compare float numbers
        time_jd = timestamps.jd
        tmin_jd = time_min.jd
        tmax_jd = time_max.jd

        t_mask = (time_jd >= tmin_jd) & (time_jd < tmax_jd)

        # Send some warning if the user asks for time range that are outside
        if ~np.any(t_mask):
            log.warning(
                f'Empty time selection for NDAPointing. Requested values are outside {timestamps.min().isot} and {timestamps.max().isot}.'
            )
        else:
            first_idx = np.where(t_mask)[0][0]
            if first_idx != 0:
                # include the previous time step defining a pointing that is still valid at tmin
                t_mask[first_idx - 1] = True

        log.debug(f'Time selection: {t_mask.sum()}/{timestamps.size}.')

        return t_mask
# ------------------------------------------------------------- #
# ============================================================= #

