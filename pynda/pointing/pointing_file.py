#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    NDA pointing functions to read the different files
"""


__author__ = 'Alan Loh'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'read_pointing_log',
    'read_pointing_pou',
    'POINTING_FILE_PATTERN',
    'find_pointing_files',
    'parse_pointing_file'
]


import numpy as np
import re
import os
import glob
from typing import Tuple
import astropy.units as u
from astropy.time import Time, TimeDelta

from pynda import DEFAULT_POINTING_FREQUENCY, SOURCE_CODE

import logging
log = logging.getLogger(__name__)


# ============================================================= #
# Functions to read NDA pointing files
# ------------------------------------------------------------- #
def read_pointing_log(file_name: str) -> np.ndarray:
    """ """
    pointing_struct = [
        ("source", "U1"),
        ("hour", "U12"),
        ("phase_right", "uint"),
        ("rew_right", "uint"),
        ("rns_right", "uint"),
        ("f1_right", "uint"),
        ("f2_right", "uint"),
        ("f3_right", "uint"),
        ("f4_right", "uint"),
        ("f5_right", "uint"),
        ("at_right", "U2"),
        ("phase_left", "uint"),
        ("rew_left", "uint"),
        ("rns_left", "uint"),
        ("f1_left", "uint"),
        ("f2_left", "uint"),
        ("f3_left", "uint"),
        ("f4_left", "uint"),
        ("f5_left", "uint"),
        ("at_left", "U2"),
        ("rir", "uint")
    ]

    def patch_phase_str(x):
        """Some 'phase_right' and 'phase_left' columns may contain
            a series of letter instead of the pointing code.
        """
        try:
            x = float(x)
            return x 
        except ValueError:
            log.debug(f"Value {x} converted to '00000000'.")
            return float("00000000")

    return np.loadtxt(
        file_name,
        dtype=np.dtype(pointing_struct),
        skiprows=1,
        converters={ 
            2: lambda x: patch_phase_str(x),
            11: lambda x: patch_phase_str(x),
        },
        delimiter=" ",
        usecols=tuple(range(len(pointing_struct))),
        comments="#"
    )


def read_pointing_pou(file_name: str) -> np.ndarray:
    """ """
    pointing_struct = [
        ("date", "U8"),
        ("hour", "U12"),
        ("phase_right", "uint"),
        ("rew_right", "uint"),
        ("rns_right", "uint"),
        ("f1_right", "uint"),
        ("f2_right", "uint"),
        ("f3_right", "uint"),
        ("f4_right", "uint"),
        ("f5_right", "uint"),
        ("at_right", "U2"),
        ("phase_left", "uint"),
        ("rew_left", "uint"),
        ("rns_left", "uint"),
        ("f1_left", "uint"),
        ("f2_left", "uint"),
        ("f3_left", "uint"),
        ("f4_left", "uint"),
        ("f5_left", "uint"),
        ("at_left", "U2"),
        ("rir", "uint")
    ]
    rfile = open(file_name, "r", encoding="utf-8", errors="ignore")
    return np.loadtxt(
        rfile,
        dtype=np.dtype(pointing_struct),
        skiprows=1,
        delimiter=" ",
        usecols=tuple(range(len(pointing_struct))),
        comments="#"
    )
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Dictionary tracking the syntax and reading functions for
# the various pointing file formats
# ------------------------------------------------------------- #
POINTING_FILE_PATTERN = {
    'log_file': {
        'pattern': r'P(?P<year>(\d{2}))(?P<month>(\d{2}))(?P<day>(\d{2}))(?P<n>(_\d{1})?).log',
        'exemple': 'PYYMMDD.log',
        'extension': '.log',
        'prefix': 'P',
        'reading_function': read_pointing_log
    },
    'pou_file': {
        'pattern': r'((?P<source>J|A|S|M|C|V|T|K))(?P<year>(\d{2}))(?P<month>(\d{2}))(?P<day>(\d{2}))(?P<n>(_\d{1})?).POU',
        'exemple': 'JYYMMDD.POU',
        'extension': '.POU',
        'prefix': '',
        'reading_function': read_pointing_pou
    }
}
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# match_pointing_file_name
# ------------------------------------------------------------- #
def match_pointing_file_name(file_name: str) -> Tuple[str, str, dict]:
    # Search a standard file pattern to determine the file format
    for file_type in POINTING_FILE_PATTERN.keys():
        match = re.match(
            POINTING_FILE_PATTERN[file_type]['pattern'],
            os.path.basename(file_name)
        )
        
        if match:
            log.info(f"Pointing file of type '{file_type}' found.")
            
            # Understand the date from the file name
            name_parsed = match.groupdict()
            date = f"20{name_parsed['year']}-{name_parsed['month']}-{name_parsed['day']}"
            log.info(f"Date '{date}' understood from the file name '{file_name}'.")

            # Stop the search among the available formats
            break
        
    else:
        # The loop has gone through all available formats without success
        raise ValueError(
            f"Impossible to parse the date from the file name '{file_name}'. Unrecognized pointing file."
        )
    return file_type, date, name_parsed
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# find_pointing_files
# ------------------------------------------------------------- #
def find_pointing_files(dir_path: str, file_type: str, time_min: Time, time_max: Time) -> list:

    # Search for the pointing files within the time range
    if not os.path.isdir(dir_path):
        raise FileNotFoundError(f"'{dir_path}' is not a valid directory.")
    
    if file_type not in POINTING_FILE_PATTERN:
        raise ValueError(f'Unavailable file_type, should be one of {POINTING_FILE_PATTERN.keys()}.')
    ext = POINTING_FILE_PATTERN[file_type]['extension']
    prefix = POINTING_FILE_PATTERN[file_type]['prefix']

    # Prepare the patterns to look for
    one_day = TimeDelta(1, format='jd')
    file_patterns = []
    current_day = Time(time_min.isot.split('T')[0]) # day at midnight
    while current_day < time_max:
        date = ''.join(current_day.isot.split('T')[0].split('-'))[2:]
        file_patterns.append(f'{prefix}*{date}{ext}')
        current_day += one_day

    # Search files
    pointing_files = []
    for file_pattern in file_patterns:
        file_found = glob.glob(os.path.join(dir_path, file_pattern))
        if len(file_found) > 1:
            log.warning(f"More than one file found, for the pattern '{file_pattern}' in '{dir_path}'.")
        elif len(file_found) == 0:
            log.warning(f"File not found for the pattern '{file_pattern}' in '{dir_path}'.")
            continue
        [pointing_files.append(fi) for fi in file_found]


    log.info(f"{len(pointing_files)} files found in '{dir_path}' for the time range {time_min.isot} - {time_max.isot}.")
    return pointing_files
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Main reading function
# ------------------------------------------------------------- #
def parse_pointing_file(file_name: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray, str]:
    """ """
    log.info(f"Reading '{file_name}'...")

    file_type, date, name_parsed = match_pointing_file_name(file_name=file_name)
    
    # If we're still here, we can parse the file with the correct reading function
    reading_function = POINTING_FILE_PATTERN[file_type]["reading_function"]
    pointing = reading_function(file_name)

    # Extract other metadata from the pointing file depending on the format
    if file_type == "log_file":
        # Associate the source code name with the source full name,
        # set it to "A"<->"Other" if the code name is not recognized.
        objects = np.array(list(map(lambda x: SOURCE_CODE.get(x, "A"), pointing["source"])))
        opt_freq = DEFAULT_POINTING_FREQUENCY
        
    elif file_type == "pou_file":
        source = SOURCE_CODE[name_parsed["source"]]
        objects = np.array([source]*pointing["date"].size)
        # Try to read the optimization frequency from the file header
        try:
            with open(file_name, "r", encoding="latin-1") as rfile:
                line = 0
                pattern = r"optimisation: (?P<frequency>(\d+)) MHz"
                while line < 10:
                    match = re.search(pattern, rfile.readline())
                    if match:
                        break
                    line += 1
            opt_freq = int(match.groupdict()["frequency"])*u.MHz
            log.info(
                f"Pointing optimization frequency from file header is {opt_freq}."
            )
        except:
            log.warning(
                f"Could not read optimization frequency from '{file_name}'."
            )
            opt_freq = DEFAULT_POINTING_FREQUENCY

    else:
        raise NotImplementedError(f"Pointing file type '{file_type}' unrecognized.")

    return pointing, objects, opt_freq, date
# ------------------------------------------------------------- #
# ============================================================= #

