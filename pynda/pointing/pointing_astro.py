#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    NDA pointing functions and tools
"""


__author__ = 'Alan Loh'
__copyright__ = 'Copyright 2022, pynda'
__credits__ = ['Alan Loh']
__maintainer__ = 'Alan'
__email__ = 'alan.loh@obspm.fr'
__status__ = 'Production'
__all__ = [
    'hadec_to_order',
    'order_to_hadec',
    'hadec_to_radec',
    'order_to_radec',
    'radec_to_altaz'
]


import astropy.units as u
from astropy.time import Time
from astropy.coordinates import Angle, Longitude, SkyCoord, AltAz
import numpy as np
import sympy as sy
from typing import Tuple

from pynda import NDA_LOCATION, DEFAULT_POINTING_FREQUENCY

import logging
log = logging.getLogger(__name__)


# ============================================================= #
# ------------------------------------------------------------- #
def hadec_to_order(
        hour_angle: u.Quantity,
        declination: u.Quantity,
        frequency: u.Quantity,
        array: str = 'left'
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """ Converts (Hour Angles, Declinations) to NDA pointing orders
        at a given optimization frequency.
    """

    if hour_angle.isscalar:
        hour_angle = hour_angle.reshape((1,))
    if declination.isscalar:
        declination = declination.reshape((1,))
    ha = hour_angle.to(u.rad).value
    dec = declination.to(u.rad).value
    fopt = frequency.to(u.MHz).value
    nda_lat = NDA_LOCATION.lat.rad

    sc = -np.cos(nda_lat)*np.sin(dec) + np.sin(nda_lat)*np.cos(dec)*np.cos(ha) - 0.5
    ss = -np.cos(dec)*np.sin(ha)
    ij = np.arange(8)//2
    ik = np.arange(8)%2

    # Phase
    p = sc[:, None]*ij[None, :]*0.2000000 + ss[:, None]*ik[None, :]*0.1733333
    if array.lower() == 'right':
        # Phase order
        phase = (np.round(p*fopt)%8).astype(int)
    elif array.lower() == 'left':
        phase = ((np.round(p*fopt)%8).astype(int) + 7)%8
    else:
        raise ValueError("Array '{array}' unknown.")
    # Reassemble the codes
    phase = np.array(list(map( lambda x: int(''.join(x.astype(str))), phase)))

    # REW
    rew = np.trunc(63.5 - 80*ss).astype(int)
    rew[rew < 0] = 0
    rew[rew > 126] = 126
    rew[rew > 63] += 1

    # RNS
    rns = np.trunc(127.5 - 150*sc).astype(int)
    rns[rns < 0] = 0
    rns[rns > 254] = 254
    rns[rns > 127] += 1

    # RIR
    rir = np.trunc(127.5 - 120*ss).astype(int)
    rir[rir < 0] = 0
    rir[rir > 254] = 254
    rir[rir > 127] += 1

    return phase, rew, rns, rir
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# ------------------------------------------------------------- #
def _show_healpix_order(
        nside: int = 128,
        frequency: u.Quantity = DEFAULT_POINTING_FREQUENCY
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
        returns (order, rew, rns)
    """
    import healpy as hp
    has, decs = hp.pix2ang(
        nside=nside,
        ipix=np.arange(hp.nside2npix(nside), dtype=int),
        lonlat=True
    )
    has = Angle(has, "deg")
    # has.wrap_at('180d', inplace=True)
    decs = Angle(decs, "deg")

    return hadec_to_order(
        hour_angle=has,
        declination=decs,
        frequency=frequency,
        array="left"
    )
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# ------------------------------------------------------------- #
def order_to_hadec(rir: int, rns: int) -> Tuple[u.Quantity, u.Quantity]:
    """
        returns (hour_angle, declination)
    """
    log.info(
        "Solving pointing order equations to get (hour_angle, declination)..."
    )
    if np.isscalar(rir) and np.isscalar(rns):
        rir = np.array([rir])
        rns = np.array([rns])
    
    # rew[rew > 63] -= 1
    rir[rir > 127] -= 1
    rns[rns > 127] -= 1
    # ss_array = (63.5 - rew)/80
    ss_array = (127.5 - rir - 0.5)/120
    sc_array = (127.5 - rns - 0.5)/150

    nda_lat = NDA_LOCATION.lat.rad

    def solve(ss, sc):
        d, h = sy.symbols("d h")
        try:
            sol = sy.nsolve(
                (
                    sy.Eq(-sy.cos(nda_lat)*sy.sin(d) + sy.sin(nda_lat)*sy.cos(d)*sy.cos(h) - 0.5, sc),
                    sy.Eq(-sy.cos(d)*sy.sin(h), ss)
                ),
                (d, h),
                (0.8, 0),
                maxsteps=20,
                prec=5
            )
            return [float(sol[1, 0])%(2*np.pi), float(sol[0, 0])]
        except ValueError:
            return [np.nan, np.nan]

    # with logging_redirect_tqdm():
    #     hadec = np.array(list((map(solve, tqdm(ss_array), sc_array))))
    hadec = np.array(list(map(solve, ss_array, sc_array)))

    return Angle(hadec[:, 0]*u.rad), Angle(hadec[:, 1]*u.rad).wrap_at('90d', inplace=False)
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# ------------------------------------------------------------- #
def hadec_to_radec(
        hour_angle: u.Quantity,
        declination: u.Quantity,
        time: Time
    ) -> Tuple[u.Quantity, u.Quantity]:
    """ """
    log.info(
        "Converting (hour_angle, declination) to (RA, Dec)..."
    )
    lst = time.sidereal_time(
        kind='mean',
        longitude=NDA_LOCATION.lon
    )
    right_ascension = lst - Longitude(hour_angle.to(u.deg).value, unit="deg", wrap_angle=180*u.deg)
    return right_ascension.deg*u.deg, declination
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# ------------------------------------------------------------- #
def order_to_radec(rir: int, rns: int, time: Time) -> SkyCoord:
    """ """
    ha, dec = order_to_hadec(rir=rir, rns=rns)
    ra, dec = hadec_to_radec(
        hour_angle=ha,
        declination=dec,
        time=time
    )
    return SkyCoord(ra, dec)
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# ------------------------------------------------------------- #
def radec_to_altaz(radec: SkyCoord, time: Time) -> SkyCoord:
    """ """
    log.info("Converting to (RA, Dec) to (alt, az)...")
    return radec.transform_to(
        AltAz(
            obstime=time,
            location=NDA_LOCATION
        )
    )
# ------------------------------------------------------------- #
# ============================================================= #

