##### QUICK START ----------------------------
#from pynda.comparefits import COMPARE_FITS

#file=COMPARE_FITS('test.fits','J170101_Rou.fits')

#file.compare_freq()
#file.compare_time()
#file.compare_data()


from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
import logging
import sys

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)


log = logging.getLogger(__name__)

class COMPARE_FITS:
    """ Compare fits file created by Python with IDL. """


    def __init__(self, file_name_py: str, file_name_idl: str):
        self.file_name_py = file_name_py
        self.file_name_idl= file_name_idl

        self.h_py = fits.open(self.file_name_py)
        self.h_idl=fits.open(self.file_name_idl)

        log.info(f"Reading {self.file_name_py}...")
        log.info(f"Reading {self.file_name_idl}...")


    def compare_freq(self) -> None:

        """ """
        freq_py=self.h_py[1].data.field('frq')  
        freq_idl=self.h_idl[1].data.field('frq')
        delta_freq=freq_idl-freq_py
        index_error_freq=np.where(delta_freq[0,:] != 0)

        if len(index_error_freq[0][:]) > 0:
            for i in range (len(index_error_freq[0][:])):
                print(f"Frequency difference of {delta_freq[0,index_error_freq[0][i]]} between python and IDL version at index {index_error_freq[0][i]} !")

        log.info(f"{len(index_error_freq[0][:])}/{len(delta_freq[0,:])} differences for the frequency array  > {100*len(index_error_freq[0][:])/len(delta_freq[0,:])} % ERROR")
        if len(index_error_freq[0][:]) != 0: 
            max_freq=np.max(delta_freq)
            ind_error_freq=np.where(delta_freq==max_freq)
            print(f"Maximum error in frequency data array: {max_freq} , a relative error of {np.max(delta_freq)/delta_freq[0,ind_error_freq[0][0]]} \n")
    
        return [100*len(index_error_freq[0][:])/len(delta_freq[0,:]), np.max(delta_freq) ]

    
    def compare_time(self) -> None:

        """ """
        time_py=self.h_py[2].data.field('jd')  
        time_idl=self.h_idl[3].data.field('JD')
        delta_time=time_idl-time_py
        index_error_time=np.where(delta_time != 0)
        
        if len(index_error_time[0][:]) > 0:
            for i in range (len(index_error_time[0][:])):
                print(f"Time difference of {delta_time[index_error_time[0][i]]} between python and IDL version at index {index_error_time[0][i]} !")

        log.info(f"{len(index_error_time[0][:])}/{len(delta_time)} differences for the time array > {100*len(index_error_time[0][:])/len(delta_time)} % ERROR " )
        if len(index_error_time[0][:]) != 0: 
            max_time=np.max(delta_time)
            ind_error_time=np.where(delta_time==max_time)
            print(f"Maximum error in time data array: {max_time} , a relative error of {np.max(delta_time)/delta_time[ind_error_time[0][0]]}\n " )
    
    def compare_data_jup(self) -> None:

        """ """
        data_py=self.h_py[2].data.field('data')  
        data_idl=self.h_idl[3].data.field('DATA')
        LL_py=data_py[:,:, 0]
        LL_idl=data_idl[:,0, :]
        RR_py=data_py[:,:, 3]
        RR_idl=data_idl[:,3, :]
        Re_cross_py=data_py[:,:, 1]
        Re_cross_idl=data_idl[:,1, :]
        Im_cross_py=data_py[:,:, 2]
        Im_cross_idl=data_idl[:,2, :]

        delta_LL=LL_idl - LL_py
        delta_RR=RR_idl - RR_py
        delta_Re_cross=Re_cross_idl - Re_cross_py
        delta_Im_cross=Im_cross_idl - Im_cross_py

        correct_LL=np.count_nonzero(delta_LL == 0)
        correct_RR=np.count_nonzero(delta_RR == 0)
        correct_Re_cross=np.count_nonzero(delta_Re_cross == 0)
        correct_Im_cross=np.count_nonzero(delta_Im_cross == 0)

        error_LL=(len(delta_LL[:,0])*len(delta_LL[0,:]))-correct_LL
        error_RR=(len(delta_RR[:,0])*len(delta_RR[0,:]))-correct_RR
        error_Re_cross=(len(delta_Re_cross[:,0])*len(delta_Re_cross[0,:]))-correct_Re_cross
        error_Im_cross=(len(delta_Im_cross[:,0])*len(delta_Im_cross[0,:]))-correct_Im_cross
        
        print('\n')
        log.info(f"{error_LL}/{len(delta_LL[:,0])*len(delta_LL[0,:])} differences for the LL data table > {(100*error_LL)/(len(delta_LL[:,0])*len(delta_LL[0,:]))} % ERROR")
        if error_LL != 0: 
            max_LL=np.max(delta_LL)
            ind_error_LL=np.where(delta_LL==max_LL)
            print(f"Maximum error in LL data table: {max_LL} , a relative error of {np.max(delta_LL)/LL_idl[ind_error_LL[0][0],ind_error_LL[1][0]]} \n")

        log.info(f"{error_RR}/{len(delta_RR[:,0])*len(delta_RR[0,:])} differences for the RR data table > {(100*error_RR)/(len(delta_RR[:,0])*len(delta_RR[0,:]))} % ERROR")
        if error_RR != 0: 
            max_RR=np.max(delta_RR)
            ind_error_RR=np.where(delta_RR==max_RR)
            print(f"Maximum error in RR data table: {max_RR} , a relative error of {np.max(delta_RR)/RR_idl[ind_error_RR[0][0],ind_error_RR[1][0]]}\n ")

        log.info(f"{error_Re_cross}/{len(delta_Re_cross[:,0])*len(delta_Re_cross[0,:])} differences for the Re_cross data table > {(100*error_Re_cross)/(len(delta_Re_cross[:,0])*len(delta_Re_cross[0,:]))} % ERROR")
        if error_Re_cross != 0: 
            max_Re_cross=np.max(delta_Re_cross)
            ind_error_Re_cross=np.where(delta_Re_cross==max_Re_cross)
            print(f"Maximum error in Re_cross data table: {max_Re_cross} , a relative error of {np.max(delta_Re_cross)/Re_cross_idl[ind_error_Re_cross[0][0],ind_error_Re_cross[1][0]]} \n")
        
        log.info(f"{error_Im_cross}/{len(delta_Im_cross[:,0])*len(delta_Im_cross[0,:])} differences for the Im_cross data table > {(100*error_Im_cross)/(len(delta_Im_cross[:,0])*len(delta_Im_cross[0,:]))} % ERROR")
        if error_Im_cross != 0: 
            max_Im_cross=np.max(delta_Im_cross)
            ind_error_Im_cross=np.where(delta_Im_cross==max_Im_cross)
            print(f"Maximum error in Im_cross data table: {max_Im_cross} , a relative error of {np.max(delta_Im_cross)/Im_cross_idl[ind_error_Im_cross[0][0],ind_error_Im_cross[1][0]]} \n")
        



    def compare_data_sun(self) -> None:

        """ """
        data_py=self.h_py[2].data.field('data')  
        data_idl=self.h_idl[3].data.field('DATA')
        LL_py=data_py[:,:, 0]
        LL_idl=data_idl[:,0, :]
        RR_py=data_py[:,:, 1]
        RR_idl=data_idl[:,1, :]

        delta_LL=LL_idl - LL_py
        delta_RR=RR_idl - RR_py


        correct_LL=np.count_nonzero(delta_LL == 0)
        correct_RR=np.count_nonzero(delta_RR == 0)


        error_LL=(len(delta_LL[:,0])*len(delta_LL[0,:]))-correct_LL
        error_RR=(len(delta_RR[:,0])*len(delta_RR[0,:]))-correct_RR

        
        print('\n')
        log.info(f"{error_LL}/{len(delta_LL[:,0])*len(delta_LL[0,:])} differences for the LL data table > {(100*error_LL)/(len(delta_LL[:,0])*len(delta_LL[0,:]))} % ERROR")
        if error_LL != 0: 
            max_LL=np.max(delta_LL)
            ind_error_LL=np.where(delta_LL==max_LL)
            print(f"Maximum error in LL data table: {max_LL} , a relative error of {np.max(delta_LL)/LL_idl[ind_error_LL[0][0],ind_error_LL[1][0]]} \n")

        log.info(f"{error_RR}/{len(delta_RR[:,0])*len(delta_RR[0,:])} differences for the RR data table > {(100*error_RR)/(len(delta_RR[:,0])*len(delta_RR[0,:]))} % ERROR")
        if error_RR != 0: 
            max_RR=np.max(delta_RR)
            ind_error_RR=np.where(delta_RR==max_RR)
            print(f"Maximum error in RR data table: {max_RR} , a relative error of {np.max(delta_RR)/RR_idl[ind_error_RR[0][0],ind_error_RR[1][0]]}\n ")



    def compare_data_mefisto(self) -> None:

        """ """
        data_py=self.h_py[2].data.field('data')  
        data_idl=self.h_idl[3].data.field('DATA')
        acc=self.h_idl[0].header['ACC']
        LL_py=data_py[:,:, 0]
        LL_idl=data_idl[:,1, :]/acc
        RR_py=data_py[:,:, 1]
        RR_idl=data_idl[:,3, :]/acc

        delta_LL=LL_idl - LL_py
        delta_RR=RR_idl - RR_py


        correct_LL=np.count_nonzero(delta_LL == 0)
        correct_RR=np.count_nonzero(delta_RR == 0)


        error_LL=(len(delta_LL[:,0])*len(delta_LL[0,:]))-correct_LL
        error_RR=(len(delta_RR[:,0])*len(delta_RR[0,:]))-correct_RR

        
        print('\n')
        log.info(f"{error_LL}/{len(delta_LL[:,0])*len(delta_LL[0,:])} differences for the LL data table > {(100*error_LL)/(len(delta_LL[:,0])*len(delta_LL[0,:]))} % ERROR")
        if error_LL != 0: 
            max_LL=np.max(delta_LL)
            ind_error_LL=np.where(delta_LL==max_LL)
            print(f"Maximum error in LL data table: {max_LL} , a relative error of {np.max(delta_LL)/LL_idl[ind_error_LL[0][0],ind_error_LL[1][0]]} \n")

        log.info(f"{error_RR}/{len(delta_RR[:,0])*len(delta_RR[0,:])} differences for the RR data table > {(100*error_RR)/(len(delta_RR[:,0])*len(delta_RR[0,:]))} % ERROR")
        if error_RR != 0: 
            max_RR=np.max(delta_RR)
            ind_error_RR=np.where(delta_RR==max_RR)
            print(f"Maximum error in RR data table: {max_RR} , a relative error of {np.max(delta_RR)/RR_idl[ind_error_RR[0][0],ind_error_RR[1][0]]}\n ")



