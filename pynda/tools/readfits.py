
##### QUICK START ----------------------------
# from readfits import READ_FITS
# from astropy.time import Time
# import astropy.units as u
# obj = READ_FITS('S220510_Rou.fits', 'jet')
# obj.plot_dynamic_spectrum("2022-05-10T14:00:14.261","2022-05-10T14:45:44.261", 20, 80, 'stokes')


from cmath import log10
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm, Normalize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from astropy.time import Time
import matplotlib.dates as mdates
import matplotlib.ticker as tick
from matplotlib import rcParams
import astropy.units as u
import logging
import sys

from sqlalchemy import null

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

log = logging.getLogger(__name__)

class READ_FITS:
    """ load fits file for quicklook. """


    def __init__(self, file_name: str, colormap_type: str = 'binary' or 'jet'):
        self.file_name = file_name
        self.hdul = fits.open(self.file_name)
        self.colormap_type=colormap_type 
        self.freq = self.hdul[1].data.field('frq')
        self.freq=self.freq[0,:]
        self.JD=self.hdul[2].data.field('jd')
        self.time=Time(self.JD, format='jd')
        self.time_dt=self.time.datetime
        self.time_iso=self.time.isot
        log.info(f"Reading {self.file_name}...")


    def plot_dynamic_spectrum(self, time_min: Time ='default' , time_max: Time ='default', freq_min: u.Quantity ='default', freq_max: u.Quantity = 'default', plot: str= 'LH/RH' or 'stokes', Smin:int = 5, Smax: int = 95, Vmax: int = 100) -> None:

        """ """

        if freq_min == 'default':
            freq_min = self.freq[0]
            
        if freq_max == 'default':
            freq_max = self.freq[-1]
            
        if time_min == 'default':
            time_min = self.time_iso[0]
        
        if time_max == 'default':
            time_max = self.time_iso[-1]
   


        freq_min_index=np.where(self.freq>=freq_min)
        freq_min_index=freq_min_index[0][0]
        freq_max_index=np.where(self.freq>=freq_max)
        freq_max_index=freq_max_index[0][0]
        self.freq=self.freq[freq_min_index:freq_max_index]

        time_min_index=np.where(self.time_iso>=time_min)
        time_min_index=time_min_index[0][0]
        time_max_index=np.where(self.time_iso>=time_max)
        time_max_index=time_max_index[0][0]
        self.time_iso=self.time_iso[time_min_index:time_max_index]
        self.time_dt=self.time_dt[time_min_index:time_max_index]


        DATA=self.hdul[2].data.field('DATA')
        LL=DATA[time_min_index:time_max_index,freq_min_index:freq_max_index,0]
        RR=DATA[time_min_index:time_max_index,freq_min_index:freq_max_index,1]
        #acc=self.hdul[0].header['acc']
        #LL=LL/acc
        #RR=RR/acc

        if plot=='stokes' :
            V=(LL-RR)/(LL+RR)
            S=10*np.log10(LL+RR)

        LL=10*np.log10(LL)
        RR=10*np.log10(RR)

        if self.file_name[0] == 'S': target ='SUN'
        else :
            if self.file_name[0] == 'J': target='JUPITER'
            else : target='OTHER'

        if self.file_name.endswith('_Rou.fits'): receiver ='NEWROUTINE'
        else :
            if self.file_name.endswith('_Spectro.fits'): receiver='MEFISTO'
            else : receiver='OTHER'
        
        log.info(f"'{target}' file")
        log.info(f"'{receiver}' receiver")
        log.info(f"Time: {self.time_dt[0]} - {self.time_dt[len(self.time_dt)-1]}")
        log.info(f"Frequency: {self.freq[0]} - {self.freq[len(self.freq)-1]}")
      #  log.info(f"Data divided by the accumulation factor {acc}.")


        #vmin= np.nanpercentile(10*np.log10(DATA/acc), Smin)
        #vmax=np.nanpercentile(10*np.log10(DATA/acc), Smax)
        vmin= np.nanpercentile(10*np.log10(DATA), Smin)
        vmax=np.nanpercentile(10*np.log10(DATA), Smax)


        if plot=='stokes':
            vmin2= np.nanpercentile(V, 0)
            vmax2= np.nanpercentile(V, Vmax)

        fig, (ax1, ax2) = plt.subplots(2)
        rcParams.update({'font.size': 30})

        plt.subplots_adjust(right=0.85,
                    left=0.1,
                    bottom=0.15,
                    top=0.85, 
                    hspace=0.5)
                    
        fig.set_size_inches(28, 18)

        if plot=='stokes':
            datax=np.transpose(S)
            datay=np.transpose(V)
            titx='S Stokes'
            tity='V Stokes'
        else :
            datax=np.transpose(LL)
            datay=np.transpose(RR)
            vmin2=vmin
            vmax2=vmax
            titx='LH flux density'
            tity='RH flux density'

        im1 = ax1.pcolormesh(self.time_dt,self.freq,datax,
            cmap=self.colormap_type,
            shading="auto",
            norm=Normalize(vmin, vmax)
        )
        cax1 = inset_axes(
            ax1,
            width='3%',
            height='100%',
            loc='lower left',
            bbox_to_anchor=(1.05, 0., 1, 1),
            bbox_transform=ax1.transAxes,
            borderpad=0,
        )

        ax1.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        fig.autofmt_xdate()
        ax1.set_xlabel(f"Time (UTC) on {self.time_iso[0][0:10]}")
        ax1.set_ylabel(f"Frequency (MHz)")
        cbar1 = plt.colorbar(im1, cax=cax1)
        cbar1.set_label(r"$dB$")        


        ax1.set_title('Nançay Decameter Array / %s' % receiver + ' / %s' % titx + ' - %s' % target, fontsize=35)

        im2 = ax2.pcolormesh(self.time_dt,self.freq,datay,
            cmap=self.colormap_type,
            shading="auto",
            norm=Normalize(vmin2, vmax2)
        )
        cax2 = inset_axes(
            ax2,
            width='3%',
            height='100%',
            loc='lower left',
            bbox_to_anchor=(1.05, 0., 1, 1),
            bbox_transform=ax2.transAxes,
            borderpad=0,
        )

        ax2.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
        fig.autofmt_xdate()
        ax2.set_xlabel(f"Time (UTC) on {self.time_iso[0][0:10]}")
        ax2.set_ylabel(f"Frequency (MHz)")
        cbar2 = plt.colorbar(im2, cax=cax2)
        if plot =="stokes" : 
            cbar2.set_label(r"$RR <> LL$")
        else : 
            cbar2.set_label(r"$dB$")
    
        ax2.set_title('Nançay Decameter Array / %s' % receiver + ' / %s' % tity + ' - %s' % target, fontsize=35)

        plt.savefig(f'Quicklook_{self.time_iso[0][0:10]}_{receiver}_{target}.png', facecolor='white')