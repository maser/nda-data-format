
##### QUICK START ----------------------------
#from readbinary import READ_BINARY
#from astropy.time import Time
#import astropy.units as u
#obj = READ_BINARY('J20170101_022612_Rou.dat', 'jet')
#obj.plot_dynamic_spectrum("2017-01-01T02:30:14.261","2017-01-01T02:40:44.261",10,80)

from pynda.newroutine import NewRoutine
from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm, Normalize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from astropy.time import Time
import matplotlib.dates as mdates
import matplotlib.ticker as tick
from matplotlib import rcParams
import astropy.units as u
import logging
import sys

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)

log = logging.getLogger(__name__)

class READ_BINARY:
    """ load fits file for quicklook. """


    def __init__(self, file_name: str, colormap_type: str = 'binary' or 'jet'):
        self.file_name = file_name

        self.nr = NewRoutine(self.file_name)
        self.colormap_type=colormap_type 


    def plot_dynamic_spectrum(self, time_min: Time = None, time_max: Time = None, freq_min: u.Quantity = None, freq_max: u.Quantity = None) -> None:

        """ """
        datax = self.nr.get(
            data_type="xx",
            time_min=Time(time_min),
            time_max=Time(time_max),
            freq_min=freq_min*u.MHz,
            freq_max=freq_max*u.MHz
        )

        datay = self.nr.get(
            data_type="yy",
            time_min=Time(time_min),
            time_max=Time(time_max),
            freq_min=freq_min*u.MHz,
            freq_max=freq_max*u.MHz
        )

        if self.file_name[0] == 'S': target ='SUN'
        else :
            if self.file_name[0] == 'J': target='JUPITER'
            else : target='OTHER'

        if self.file_name.endswith('_Rou.dat'): receiver ='NEWROUTINE'
        else :
            if self.file_name.endswith('_Spectro.dat'): receiver='MEFISTO'
            else : receiver='OTHER'
        

        fig, (ax1, ax2) = plt.subplots(2)
        rcParams.update({'font.size': 30})

        plt.subplots_adjust(right=0.85,
                left=0.1,
                bottom=0.15,
                top=0.85, 
                hspace=0.5)
                    
        fig.set_size_inches(28, 18)

        datax.plot_dynamic_spectrum(log=False, cmap=self.colormap_type, fig_ax=(fig, ax1))
        ax1.set_title('Nançay Decameter Array - %s' % receiver + '- %s - Left Handed Polarization' % target, fontsize=35)

        datay.plot_dynamic_spectrum(log=False, cmap=self.colormap_type, fig_ax=(fig, ax2))
        ax2.set_title('Nançay Decameter Array - %s' % receiver + '- %s - Right Handed Polarization' % target, fontsize=35)

        plt.savefig(f'Quicklook_{self.nr.time.isot[0][0:10]}_{receiver}_{target}.png', facecolor='white')