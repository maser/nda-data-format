#! /usr/bin/python3
# -*- coding: utf-8 -*-


__author__ = "Alan Loh, Alexia Duchene, Patrice Renaud"
__copyright__ = "Copyright 2022, pynda"
__credits__ = ["Alan Loh", "Alexia Duchene", "Patrice Renaud"]
__license__ = "MIT"
__version__ = "0.4.13"
__maintainer__ = "Alan Loh"
__email__ = "alan.loh@obspm.fr"


import logging
import sys
import numpy as np

import astropy.units as u
from astropy.coordinates import EarthLocation


# ============================================================= #
# Logging configuration
# ------------------------------------------------------------- #
logging.basicConfig(
    stream=sys.stdout,
    level=logging.WARNING,
    format='%(asctime)s | %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
)
log = logging.getLogger(__name__)
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# NDA Array
# ------------------------------------------------------------- #
LATITUDE_NDA = 47.380510*u.deg #47.379998037520146*u.deg
LONGITUDE_NDA = 2.193226*u.deg
ALTITUDE_NDA = 135*u.m
NDA_LOCATION = EarthLocation(lon=LONGITUDE_NDA, lat=LATITUDE_NDA, height=ALTITUDE_NDA)
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Other
# ------------------------------------------------------------- #
NDA_RECEIVERS = {
    'newroutine': {
        'name': 'NewRoutine',
        'acronym': 'NR',
        'file_suffix': ['_Rou.dat', '.dat'],
        'source': {
            'jupiter': {
                'll': 0,
                're_lr': 1,
                'im_lr': 2,
                'rr': 3
            },
            'sun': {
                'll': 0,
                're_lr': 1,
                'im_lr': 2,
                'rr': 3,
                'bloc_ll': 4,
                'bloc_rr': 5
            },
            'transit': {
                'll': 0,
                'rr': 1,
                'bloc_ll': 2,
                'bloc_rr': 3
            },
        }
    },
    'mefisto': {
        'name': 'MeFiSto',
        'acronym': 'M',
        'file_suffix': ['_Spectro.dat'],
        'source': {
            'sun': {
                'll_raw': 0,
                'll': 1,
                'rr_raw': 2,
                'rr': 3
            }
        }
    }
}

NDA_FITS_DATA_COLUMNS = {
    'newroutine': {
        'sun': np.array([0, 3]),
        'jupiter': np.array([0, 3, 1, 2]), #(LH, reRL, imRL, RH,) --> (LH, RH, reRL, imRL)
        'transit': np.array([0, 1]) #(LH, RH, blocLH, blocRH) --> (LH, RH)
    },
    'mefisto': {
        'sun': np.array([1, 3])
    }
}

SOURCE_CODE = {
    'J': 'Jupiter',
    'S': 'Sun',
    'A': 'Other',
    'M': 'Manual',
    'C': 'CygA',
    'T': 'TauA',
    'K': 'CasA',
    'V': 'VirA'
}
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Quicklook
# ------------------------------------------------------------- #
# Matplotlib cmap for the quicklook plot, in color or black and white 
QL_PARAMETERS = {
    'color': {
        True: {
            'cmap': 'viridis',
            'acronym': 'col'
        },
        False: {
            'cmap': 'binary',
            'acronym': 'bw'
        }
    },
    'thresholds': {
        'newroutine': {
            'jupiter': {
                'smin': 5,
                'smax': 98
            },
            'sun': {
                'smin': 5,
                'smax': 95
            },
            'other': {
                'smin': 5,
                'smax': 95
            }
        },
        'mefisto': {
            'sun': {
                'smin': 5,
                'smax': 98
            }
        }
    },
    'frequency': {
        'newroutine': {
            'jupiter': {
                'freq_min': 10.009766,
                'freq_max': 39.990234
            },
            'sun': {
                'freq_min': 10.009766,
                'freq_max': 87.990234
            },
            'other': {
                'freq_min': 10.009766,
                'freq_max': 87.990234
            }
        },
        'mefisto': {
            'sun': {
                'freq_min': 10.009766,
                'freq_max': 34.954426
            }
        } 
    },
    'time_size': {
        'min': 1500,
        'max': 4000
    }
}
# ------------------------------------------------------------- #
# ============================================================= #


# ============================================================= #
# Pointing
# ------------------------------------------------------------- #
DEFAULT_POINTING_FREQUENCY = 25*u.MHz # to compute the pointing

# Last pointing step
LAST_POINTING_FREQUENCY = 30*u.MHz
LAST_POINTING_DECLINATION = 20*u.deg
LAST_POINTING_HOUR_ANGLE = 0*u.deg

# Frequency filters
FIELD_FILTERS = {
    '1': [8, 82],
    '2': [8, 45],
    '3': [16, 45],
    '4': [24, 82]
}
LAB_FILTER_INDICES = {
    'newroutine': 2,
    'juno': 3,
    'mefisto': 4,
    'routine': 5
}
LAB_FILTERS = {
    '1': 0,
    '2': 11,
    '3': 14,
    '4': 18
}
# ------------------------------------------------------------- #
# ============================================================= #

