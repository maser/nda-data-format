#! /usr/bin/python3
# -*- coding: utf-8 -*-


from pynda.pointing.pointing_astro import (
    hadec_to_order,
    order_to_hadec,
    hadec_to_radec
)
from pynda.pointing import NDAPointing
import os
import pytest
import astropy.units as u
from astropy.time import Time, TimeDelta
from astropy.coordinates import SkyCoord
import numpy as np


DATA_DIRECTORY = os.path.join(os.path.dirname(__file__), 'data')


def test_file_not_found() -> None:
    """ Dummy file name. """
    file_name = 'not_existing.txt'
    with pytest.raises(FileNotFoundError):
        _ = NDAPointing.from_file(file_name)


def test_invalid_file() -> None:
    """ Existing file, but not a pointing file. """
    file_name = os.path.join(DATA_DIRECTORY, 'S220422_Rou.fits')
    with pytest.raises(Exception):
        _ = NDAPointing.from_file(file_name)


# ============================================================= #
# Testing POU files #
@pytest.fixture(scope='module')
def pou_pointing() -> NDAPointing:
    """ Open the POU file and return it for other tests. """
    file_name = os.path.join(DATA_DIRECTORY, 'J220423.POU')
    return NDAPointing.from_file(file_name)


def test_pou_file_opt_frequency(pou_pointing) -> None:
    """ Checks that the optimization frequency has been parsed from the POU file header. """
    # All opt_freq should be at 25 MHz
    assert np.all(pou_pointing.opt_frequency[:-1] == 25*u.MHz)
    # The last one should be at 30 MHz, while closing the observation
    assert pou_pointing.opt_frequency[-1] == 30*u.MHz


def test_pou_file_source(pou_pointing) -> None:
    """ Checks that the object is correct """
    assert np.all(pou_pointing.source == 'Jupiter')


def test_pou_file_time_format(pou_pointing) -> None:
    """ Checks that this is an astropy time. """
    assert isinstance(pou_pointing.time, Time)


def test_pou_file_time_values(pou_pointing) -> None:
    """ Checks that the beginning and end times are correct. """
    assert pou_pointing.time[0].isot == '2022-04-23T05:33:54.000'
    assert pou_pointing.time[-1].isot == '2022-04-23T13:33:54.000'


# ============================================================= #
# Testing LOG files #
@pytest.fixture(scope='module')
def log_pointing() -> NDAPointing:
    """ Open the LOG file and return it for other tests. """
    file_name = os.path.join(DATA_DIRECTORY, "P170101.log")
    return NDAPointing.from_file(file_name)


def test_log_file_opt_frequency(log_pointing) -> None:
    """ Checks that the optimization frequency is set to default. """
    # All opt_freq should be at 25 MHz
    mask = np.ones(log_pointing.size, dtype=bool)
    mask[np.array([507, -1])] = False
   # assert np.all(log_pointing.opt_frequency[:-1] == 25*u.MHz)
    assert np.all(log_pointing.opt_frequency[mask] == 25*u.MHz)
    # The last one should be at 30 MHz, while closing the observation
    assert log_pointing.opt_frequency[-1] == 30*u.MHz


def test_log_file_source(log_pointing) -> None:
    """ Checks that the object is correct """
    assert np.all(log_pointing.source[:508] == 'Jupiter')
    assert np.all(log_pointing.source[508:] == 'Sun')


def test_pou_file_time_format(log_pointing) -> None:
    """ Checks that this is an astropy time. """
    assert isinstance(log_pointing.time, Time)


def test_pou_file_time_values(log_pointing) -> None:
    """ Checks that the beginning and end times are correct. """
    assert log_pointing.time[0].isot == "2017-01-01T02:26:59.272" 
    assert log_pointing.time[-1].isot == "2017-01-01T15:52:00.000"


# ============================================================= #
# Testing coordinates / order conversion #
@pytest.mark.skip(reason="Theoretical Jupiter HA and Dec seems to differ from what was used by the NDA scripts")
def test_hadec_to_order() -> None:
    """ Checks that Jupiter coordinates converted to orders matches the J220423.POU file.
        
        # Computing the coordinates:
        from pynda.ndapointing import NDAPointing
        from nenupy.astro.target import SolarSystemTarget
        ndap = NDAPointing("./tests/data/J220423.POU")
        jupiter = SolarSystemTarget.from_name("Jupiter", ndap.time[::30]
        jupiter.hour_angle(), jupiter.coordinates.dec
    """
    jupiter_ha = [
        300.14147776, 307.74137237, 315.21599914, 321.77196029, 328.95429105,
        335.59377433, 343.15193154, 349.45735784, 357.18255107, 3.40446489,
        10.75384071, 16.68345097, 23.57349106, 30.50528858, 37.22829601,
        44.45239573, 50.71606416, 58.48301041
    ]*u.deg
    jupiter_dec = [
        -2.54161024, -2.53984202, -2.53810303, -2.53657782, -2.53490693, -2.53336235,
        -2.53160406, -2.5301372, -2.52834004, -2.52689257, -2.52518279, -2.52380329,
        -2.52220032, -2.52058762, -2.51902349, -2.51734276, -2.51588549, -2.51407849
    ]*u.deg

    orders_left = np.array([
        73626251, 72727272, 72727303, 72020313, 71021314, 71021324, 70011324,
        70012234, 77012233, 77002133, 76072132, 76071031, 75061720, 75061617,
        74050616, 74740505, 74747474, 73736262
    ])
    rew_left = np.array([
        0, 0, 7, 13, 22, 30, 40, 48, 59, 68, 79, 87, 96, 104, 112, 120, 126, 127])

    rns_left = np.array([144, 131, 120, 111, 103, 97, 92, 89, 88, 88, 89, 92, 96, 102, 110, 119, 127, 140])

    order, rew, rns = hadec_to_order(
        hour_angle=jupiter_ha,
        declination=jupiter_dec,
        frequency=25*u.MHz
    )

    np.testing.assert_array_equal(order, orders_left)
    np.testing.assert_array_equal(rew, rew_left)
    np.testing.assert_array_equal(rns, rns_left)


def test_order_to_hadec_conversion_1() -> None:
    """
        times = Time("2022-01-01 10:00:00") + np.arange(7)*TimeDelta(3600, format="sec")
        src = FixedTarget(coordinates=SkyCoord(300, -10, unit="deg"), time=times)
    """
    # ra=300, dec=-10deg source 
    hour_angles = [
        313.23311651, 328.27418526, 343.31525385, 358.35632243, 13.39739118,
        28.43845977, 43.47952835
    ]*u.deg
    dec = [-10, -10, -10, -10, -10, -10, -10]*u.deg
    s1 = SkyCoord(hour_angles, dec)

    _, rew, rns, rir = hadec_to_order(
        hour_angle=hour_angles,
        declination=dec,
        frequency=25*u.MHz
    )

    ha_computed, dec_computed = order_to_hadec(rir, rns)
    s2 = SkyCoord(ha_computed, dec_computed)

    assert np.all( s1.separation(s2).deg < 1.5 )


def test_order_to_hadec_conversion_2() -> None:
    """
        times = Time("2022-01-01 08:00:00") + np.arange(10)*TimeDelta(3600, format="sec")
        src = FixedTarget.from_name("Cyg A", times)
    """
    # Cyg A 
    hour_angles = [
        283.28282743, 298.32389601, 313.3649646,  328.40603335, 343.44710194,
        358.48817052, 13.52923927, 28.57030786, 43.61137644, 58.65244519
    ]*u.deg
    dec = np.repeat(40.73391574, 10)*u.deg
    s1 = SkyCoord(hour_angles, dec)

    _, rew, rns, rir = hadec_to_order(
        hour_angle=hour_angles,
        declination=dec,
        frequency=25*u.MHz
    )

    ha_computed, dec_computed = order_to_hadec(rir, rns)
    s2 = SkyCoord(ha_computed, dec_computed)

    assert np.all( s1.separation(s2).deg < 1.5 )


def test_order_to_hadec_conversion_3() -> None:
    """
        times = Time("2022-01-01 03:00:00") + np.arange(10)*TimeDelta(3600, format="sec")
        src = FixedTarget.from_name("Vir A", times)
    """
    # Vir A 
    hour_angles = [
        320.23970532, 335.28077391, 350.32184266, 5.36291124, 20.40397983,
        35.44504858, 50.48611717
    ]*u.deg
    dec = np.repeat(12.391123293917, 7)*u.deg
    s1 = SkyCoord(hour_angles, dec)

    _, rew, rns, rir = hadec_to_order(
        hour_angle=hour_angles,
        declination=dec,
        frequency=25*u.MHz
    )

    ha_computed, dec_computed = order_to_hadec(rir, rns)
    s2 = SkyCoord(ha_computed, dec_computed)

    assert np.all( s1.separation(s2).deg < 1.5 )


def test_order_to_hadec_conversion_4() -> None:
    """
        times = Time("2022-01-01 14:00:00") + np.arange(6)*TimeDelta(3600, format="sec")
        src = FixedTarget.from_name("Cas A", times)

        Visualize:
        times = Time("2022-01-01 14:00:00") + np.arange(6)*TimeDelta(3600, format="sec")
        src = FixedTarget.from_name("Cas A", times)
        order, rews, rnss = hadec_to_order(
            hour_angle=src.hour_angle(),
            declination=src.coordinates.dec,
            frequency=25*u.MHz
        )
        for ew, ns in zip(rews, rnss):
            ha, dec = order_to_hadec(ew, ns)
            vals = (rrew==ew).astype(int)
            vals[rrns==ns] += 1
            hp.mollview( vals)
            hp.graticule()
            hp.projscatter(ha[0].to(u.deg).value, dec[0].to(u.deg).value, lonlat=True,)
            plt.show()
    """
    # Vir A 
    hour_angles = [
        322.54739118, 337.58845977, 352.62952835,
        7.6705971, 22.71166569, 37.75273427
    ]*u.deg
    dec = np.repeat(58.815, 6)*u.deg
    s1 = SkyCoord(hour_angles, dec)

    _, rew, rns, rir = hadec_to_order(
        hour_angle=hour_angles,
        declination=dec,
        frequency=25*u.MHz
    )

    ha_computed, dec_computed = order_to_hadec(rir, rns)
    s2 = SkyCoord(ha_computed, dec_computed)

    assert np.all( s1.separation(s2).deg < 1.5 )


def test_hadec_to_radec() -> None:
    """
        times = Time("2022-01-01 10:00:00") + np.arange(7)*TimeDelta(3600, format="sec")
        src = FixedTarget(coordinates=SkyCoord(300, -10, unit="deg"), time=times)
    """
    # ra=300, dec=-10deg source
    times = Time("2022-01-01 10:00:00") + np.arange(7)*TimeDelta(3600, format="sec")
    hour_angles = [
        313.23311651, 328.27418526, 343.31525385, 358.35632243, 13.39739118,
        28.43845977, 43.47952835
    ]*u.deg
    dec = [-10, -10, -10, -10, -10, -10, -10]*u.deg
    
    ra_computed, dec_computed = hadec_to_radec(
        hour_angle=hour_angles,
        declination=dec,
        time=times
    )
    np.testing.assert_allclose(ra_computed.to(u.deg).value, 300, rtol=1e-03)
    np.testing.assert_allclose(dec_computed.to(u.deg).value, -10, rtol=1e-03)

