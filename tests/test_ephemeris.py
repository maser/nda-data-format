#! /usr/bin/python3
# -*- coding: utf-8 -*-


from pynda.ephemeris .ephem import Jupiter, JupiterMoon, Sun
from astropy.time import Time
import pytest
import astropy.units as u


# Prepare the different ephemeris objects
@pytest.fixture(scope="module")
def jupiter_from_earth() -> Jupiter:
    """ Returns Jupiter object at the test date. """
    return Jupiter.seen_from(observer="EARTH", time=Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def io_from_earth() -> JupiterMoon:
    """ Returns Io object at the test date. """
    return JupiterMoon.IO.seen_from("EARTH", Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def ganymede_from_earth() -> JupiterMoon:
    """ Returns Ganymede object at the test date. """
    return JupiterMoon.GANYMEDE.seen_from("EARTH", Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def europa_from_earth() -> JupiterMoon:
    """ Returns Europa object at the test date. """
    return JupiterMoon.EUROPA.seen_from("EARTH", Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def callisto_from_earth() -> JupiterMoon:
    """ Returns Callisto object at the test date. """
    return JupiterMoon.CALLISTO.seen_from("EARTH", Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def sun_from_earth() -> Sun:
    """ Returns Sun object at the test date. """
    return Sun.seen_from("EARTH", Time("2021-07-19 04:12:04"))

@pytest.fixture(scope="module")
def sun_from_jupiter() -> Sun:
    """ Returns Sun object at the test date. """
    return Sun.seen_from("JUPITER", Time("2021-07-19 04:12:04"))


def test_jupiter_coordinates(jupiter_from_earth: Jupiter) -> None:
    """ Tests RA and Dec of Jupiter at a given time. """
    jupiter_coordinates = jupiter_from_earth.get_sky_coordinates()
    assert jupiter_coordinates.ra.deg == pytest.approx(333.064399765, abs=1e-2)
    assert jupiter_coordinates.dec.deg == pytest.approx(-12.2747223124, abs=1e-2)


def test_jupiter_distance(jupiter_from_earth: Jupiter) -> None:
    """ Tests Jupiter distance to the Earth at a given time. """
    jupiter_distance_au = jupiter_from_earth.target_distance.to(u.AU).value
    assert jupiter_distance_au == pytest.approx(4.16050000894, abs=1e-3)


def test_jupiter_cml(jupiter_from_earth: Jupiter) -> None:
    """ Tests Jupiter Central Meridian Longitude at a given time. """
    jupiter_cml = jupiter_from_earth.cml.deg
    assert jupiter_cml == pytest.approx(147.052429456, abs=1e0) # APIS
    assert jupiter_cml == pytest.approx(147.826833, abs=1e-2) # JPL Horizons


@pytest.mark.skip(reason="TBD.")
def test_jupiter_magnetic_latitude(jupiter_from_earth: Jupiter) -> None:
    """ Tests Jupiter magnetic latitude. """
    assert jupiter_from_earth.target_mag_latitude.deg == pytest(0) # TO DEFINE


def test_jupiter_latitude(jupiter_from_earth: Jupiter) -> None:
    """ Tests Jupiter latitude at a given time. """
    jupiter_latitude = jupiter_from_earth.target_latitude.deg
    assert jupiter_latitude == pytest.approx(0.810000002384, abs=1e-3) # APIS
    assert jupiter_latitude == pytest.approx(0.925461, abs=2e-1) # JPL Horizons


def test_io_local_time(io_from_earth: JupiterMoon) -> None:
    """ Tests Io local time at a given time. """
    io_time = io_from_earth.target_local_time.hour
    assert io_time == pytest.approx(10.7632595171, abs=0.3)


def test_io_longitude(io_from_earth: JupiterMoon) -> None:
    """ Tests Io local time at a given time. """
    io_longitude = io_from_earth.target_longitude.deg
    assert io_longitude == pytest.approx(172.324507448, abs=1e0)


def test_io_magnetic_latitude(io_from_earth: JupiterMoon) -> None:
    """ Tests Io magnetic latitude at a given time. """
    io_mag_lat = io_from_earth.target_mag_latitude.deg
    assert io_mag_lat == pytest.approx(9.11670125037, abs=1e0)


def test_ganymede_longitude(ganymede_from_earth: JupiterMoon) -> None:
    """ Tests Ganymede local time at a given time. """
    ganymede_longitude = ganymede_from_earth.target_longitude.deg
    assert ganymede_longitude == pytest.approx(96.1731885424, abs=1e0)


def test_ganymede_magnetic_latitude(ganymede_from_earth: JupiterMoon) -> None:
    """ Tests Ganymede magnetic latitude at a given time. """
    ganymede_mag_lat = ganymede_from_earth.target_mag_latitude.deg
    assert ganymede_mag_lat == pytest.approx(-1.78430320770, abs=1e0)


def test_ganymede_local_time(ganymede_from_earth: JupiterMoon) -> None:
    """ Tests Ganymede local time at a given time. """
    ganymede_time = ganymede_from_earth.target_local_time.hour
    assert ganymede_time == pytest.approx(15.8131926196, abs=0.3)


def test_europa_longitude(europa_from_earth: JupiterMoon) -> None:
    """ Tests Europa local time at a given time. """
    europa_longitude = europa_from_earth.target_longitude.deg
    assert europa_longitude == pytest.approx(60.5846983327, abs=1e0)


def test_europa_magnetic_latitude(europa_from_earth: JupiterMoon) -> None:
    """ Tests Europa magnetic latitude at a given time. """
    europa_mag_lat = europa_from_earth.target_mag_latitude.deg
    assert europa_mag_lat == pytest.approx(-7.48120337640, abs=1e0)


def test_europa_local_time(europa_from_earth: JupiterMoon) -> None:
    """ Tests Europa local time at a given time. """
    europa_time = europa_from_earth.target_local_time.hour
    assert europa_time == pytest.approx(18.1645360899, abs=0.3)


def test_callisto_longitude(callisto_from_earth: JupiterMoon) -> None:
    """ Tests Callisto local time at a given time. """
    callisto_longitude = callisto_from_earth.target_longitude.deg
    assert callisto_longitude == pytest.approx(22.8056382808, abs=1e0)


def test_callisto_magnetic_latitude(callisto_from_earth: JupiterMoon) -> None:
    """ Tests Callisto magnetic latitude at a given time. """
    callisto_mag_lat = callisto_from_earth.target_mag_latitude.deg
    assert callisto_mag_lat == pytest.approx(-9.55028937578, abs=1e0)


def test_callisto_local_time(callisto_from_earth: JupiterMoon) -> None:
    """ Tests Callisto local time at a given time. """
    callisto_time = callisto_from_earth.target_local_time.hour
    assert callisto_time == pytest.approx(20.7086551584, abs=0.3)


def test_sun_distance(sun_from_earth: Sun) -> None:
    """ Tests Sun distance at a given time. """
    sun_distance = sun_from_earth.target_distance.to(u.m).value
    assert sun_distance == pytest.approx(1.5e11, abs=1e10)




# *******************************************************************************
# Ephemeris / WWW_USER Fri Jun 10 03:33:12 2022 Pasadena, USA      / Horizons    
# *******************************************************************************
# Target body name: Jupiter (599)                   {source: jup365_merged}
# Center body name: Earth (399)                     {source: DE441}
# Center-site name: (user defined site below)
# *******************************************************************************
# Start time      : A.D. 2021-Jul-19 04:12:04.0000 UT      
# Stop  time      : A.D. 2021-Jul-19 04:12:05.0000 UT      
# Step-size       : 1 steps
# *******************************************************************************
# Target pole/equ : IAU_JUPITER                     {West-longitude positive}
# Target radii    : 71492.0 x 71492.0 x 66854.0 km  {Equator, meridian, pole}    
# Center geodetic : 2.19322600,47.3805100,0.1350000 {E-lon(deg),Lat(deg),Alt(km)}
# Center cylindric: 2.19322600,4326.74507,4670.6118 {E-lon(deg),Dxy(km),Dz(km)}
# Center pole/equ : ITRF93                          {East-longitude positive}
# Center radii    : 6378.1 x 6378.1 x 6356.8 km     {Equator, meridian, pole}    
# Target primary  : Sun
# Vis. interferer : MOON (R_eq= 1737.400) km        {source: DE441}
# Rel. light bend : Sun, EARTH                      {source: DE441}
# Rel. lght bnd GM: 1.3271E+11, 3.9860E+05 km^3/s^2                              
# Atmos refraction: NO (AIRLESS)
# RA format       : DEG
# Time format     : CAL 
# RTS-only print  : NO       
# EOP file        : eop.220608.p220901                                           
# EOP coverage    : DATA-BASED 1962-JAN-20 TO 2022-JUN-08. PREDICTS-> 2022-AUG-31
# Units conversion: 1 au= 149597870.700 km, c= 299792.458 km/s, 1 day= 86400.0 s 
# Table cut-offs 1: Elevation (-90.0deg=NO ),Airmass (>38.000=NO), Daylight (NO )
# Table cut-offs 2: Solar elongation (  0.0,180.0=NO ),Local Hour Angle( 0.0=NO )
# Table cut-offs 3: RA/DEC angular rate (     0.0=NO )                           
# *******************************************************************************************************************************************************************************************************************************************************************
#  Date__(UT)__HR:MN:SC.fff     R.A.___(ICRF)___DEC  R.A._(a-appar)_DEC.    APmag   S-brt  ObsSub-LON ObsSub-LAT  SunSub-LON SunSub-LAT             delta      deldot     S-O-T /r     S-T-O  L_Ap_Hour_Ang  Sky_motion  Sky_mot_PA  RelVel-ANG  Lun_Sky_Brt  sky_SNR
# *******************************************************************************************************************************************************************************************************************************************************************
# $$SOE
#  2021-Jul-19 04:12:04.000 *   333.06417 -12.27523  333.35255 -12.16835   -2.766   5.346  147.827280   0.925894  154.328580   0.426325  4.16047676603104 -15.1511543  145.8062 /L    6.5203    1.935871574   0.2208927   247.22236   -53.75244         n.a.     n.a.
#  2021-Jul-19 04:12:05.000 *   333.06417 -12.27523  333.35255 -12.16835   -2.766   5.346  147.837357   0.925894  154.338655   0.426325  4.16047666475227 -15.1511300  145.8062 /L    6.5203    1.936150177   0.2208925   247.22236   -53.75241         n.a.     n.a.
# $$EOE
# *******************************************************************************************************************************************************************************************************************************************************************
