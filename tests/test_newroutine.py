import pytest
from pynda.newroutine import NDA


@pytest.mark.parametrize(
    "file_name,expected_receiver",
    [
        ("path/to/S20160102_022312_20160102_102659_Spectro.dat", "mefisto"),
        ("S20160102_022312_20160102_102659_Spectro.dat", "mefisto"),
        ("path/to/S20160102_022312_20160102_102659_Rou.dat", "newroutine"),
        ("T20160102_022312_20160102_102659_Rou.dat", "newroutine"),
        ("path/to/M20160102_022312_20160102_102659.dat", "newroutine"),
        ("A20160102_022312_20160102_102659.dat", "newroutine")
    ]
)
def test_filename_receiver(file_name, expected_receiver):
    receiver = NDA._parse_receiver_name(file_name=file_name)
    assert receiver == expected_receiver

