#! /usr/bin/python3
# -*- coding: utf-8 -*-


import pytest

from pynda.pointing.pointing_file import match_pointing_file_name


# ============================================================= #
# Tests... #
def test_match_pointing_file_name_error() -> None:
    with pytest.raises(ValueError):
        _ = match_pointing_file_name(file_name='P170101.txt')


def test_match_pointing_file_name_log() -> None:
    file_type, date, _ = match_pointing_file_name(file_name='P170101.log')
    assert file_type == 'log_file'
    assert date == '2017-01-01'


def test_match_pointing_file_name_pou() -> None:
    file_type, date, name_parsed = match_pointing_file_name(file_name='tests/data/J220423.POU')
    assert file_type == 'pou_file'
    assert date == '2022-04-23'
    assert name_parsed['source'] == 'J'

