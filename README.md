# NDA Data Format

Conversion from raw NDA data to FITS files, along with pre-processing steps such as RFI mitigation and binning for instance.

## Installation

Where ``nda-data-format/setup.py`` is:
```
pip install .
```

## Quickstart

```python

from pynda.newroutine import NDA
from pynda.pointing import NDAPointing
from astropy.time import Time
import astropy.units as u

# Read a NewRoutine raw file
nr = NDA(
    "./tests/data/J20170101_022612_Rou.dat"
)

# Read a NDA pointing file
npo = NDAPointing.from_dir(
    "./tests/data",
    time_min=nr.time_min,
    time_max=nr.time_max,
    receiver=nr.receiver
)

# Get a time/frequency cutout
data = nr.get(
    data_type="RR",
    time_min=Time("2017-01-01T02:26:14.261"),
    time_max=Time("2017-01-01T02:36:14.261"),
    freq_min=20*u.MHz
    freq_max=None
)

# Display the selected data
data.plot_dynamic_spectrum(log=True)


# Convert to standardized FITS
nr.export_fits(
    save_path="",
    pointing=npo
)

# Produce standard quicklooks
nr.plot_quicklooks(
    path_name='/my/path/', # path where the quicklook will be saved
    color=True, # produces a quicklook in colour or not 
    pdf=False # saves the quicklook in .pdf or in .png
)
```


## Testing

Simply run:

```
pytest
```

at the same level as the ``nda-data-format/tests/`` directory.
