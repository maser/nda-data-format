#! /usr/bin/python3
# -*- coding: utf-8 -*-


from setuptools import setup, find_packages
import pynda


setup(
    name="pynda",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "numpy",
        "astropy",
        "dask",
        "sunpy",
        "sympy",
        "webgeocalc>=1.4.0"
    ],
    python_requires=">=3.8",
    version=pynda.__version__,
    description="Nancay Decametric Array data reading and converting",
    url="https://gitlab.obspm.fr/maser/nda-data-format",
    author=pynda.__author__,
    author_email=pynda.__email__,
    license=pynda.__license__,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Intended Audience :: Science/Research"
    ],
    zip_safe=False
)

